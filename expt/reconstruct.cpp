// Sriramajayam

#include <ExptDataStruct.h>
#include <StereoRig.h>

// All details for images is in this file
const cv::String exptxml = "name-of-xml-file";

// Number of different patters being used
constexpr int nStages = 2;

// Max. number of levels over all stages
const expr int nMaxLevels = 4;

int main()
{
  // Parameters from experimental data
  vc::ExptDataStruct data(exptxml);
  CV_Assert(nStages==data.nStages && "Inconsistent number of stages.");
  CV_Assert(nMaxLevels==*std::max_element(data.nLevels.begin(), data.nLevels.end())
	    && "Incorrect value for maximum number of levels");
  
  // If required, organize files & folders from expt
  // record.txt contains all details of the organization
  data.Organize("record.txt");

  // Stereo calibration
  vc::StereoRig rig(data.lcamCalib, data.lcamCorners,
		    data.rcamCalib, data.rcamCorners);
  cv::String srigfile = data.workDir+cv::String("/outxml/stereo.xml");
  rig.Save(srigfile);
  
  // Create triangulator
  vc::Triangulator tri(rig);
  
  // Compute correspondence for each pose
  for(int pose=0; pose<data.nPoses; ++pose)
    {
      std::cout<<"\nComputing correspondence for pose "<<pose; std::fflush( stdout );
      
      // Files for this pose
      std::vector<cv::String> poseFiles(data.nStages);
      for(int stage=0; stage<data.nStages; ++stage)
	poseFiles[stage] =
	  data.workDir + "/inxmls/pose" +
	  cv::String(std::to_string(pose)) + "-stage" +
	  cv::String(std::to_string(stage)) + cv::String(".xml");
      
      // Create corresponder
      vc::Corresponder corr(data.imgSize, data.nLevels, poseFiles);

      // Compute correspondence
      corr.Compute<nStages, nMaxLevels>();

      // Save averaged corresponding pixels
      cv::String corrfile =
	data.workDir + cv::String("/outxmls/corr-pose") + cv::String(std::to_string(pose)) + cv::String(".xml");
      std::cout<<"\nSaving correspondence for pose "<<pose<<" to file "<<corrfile; std::fflush( stdout );
      scene.Save<nStages, nMaxLevels>(corrfile);

      // Triangulate
      cv::String ptsfile = 
	data.workDir + cv::String("/outxmls/tri-pose") + cv::String(std::to_string(pose)) + cv::String(".ply");
      std::cout<<"\nSaving triangulation for pose "<<pose<<" to file "<<ptsfile; std::fflush( stdout );
      tri.Triangulate<vc::TriMethod::Sym>(corrfile, ptsfile);
    }
}
