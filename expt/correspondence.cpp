// Sriramajayam

#include <Corresponder.h>

// Computes correspondence between a pair of scenes
// nStages: Number of different patterns.
// nStageLevels: Number of levels at each stage
// nStageLevels[i] = number of levels at stage i.
// nMaxLevels: Max(nStageLevels)

int main()
{
  // Number of different patters being used 
 constexpr int nStages = 2;

 // xml files with names of images for each stage
 std::vector<cv::String> scenefiles = 
   {"Dec31/hpattern.xml", "Dec31/vpattern.xml"};
 
 // Number of levels in each stage
 const std::vector<int> nStageLevels = {8,8};
 
 // Maximum over the levels across all stages
 constexpr int nMaxLevels = 8;
 
 // Sanity check
 CV_Assert(nStageLevels.size()==nStages);
 CV_Assert(scenefiles.size()==nStages);
 CV_Assert(nMaxLevels==*std::max_element(nStageLevels.begin(), nStageLevels.end())); 
 
 // Create corresponder
 vc::Corresponder scene(cv::Size(3984,2656), nStageLevels, scenefiles);
  
 // Compute correspondence
 scene.Compute<nStages, nMaxLevels>();

 // Saves the average pixel coordinates within each corresponding block
 scene.Save<nStages, nMaxLevels>("corresp.xml");
}
