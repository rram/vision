// Sriramajayam

#include <Triangulator.h>
#include <fstream>

int main()
{
  // Create rig
  vc::StereoRig rig("DEC31/stereorig.xml");

  // Create triangulator
  vc::Triangulator tri(rig);

  // triangulate with a specified method
  tri.Triangulate<vc::TriMethod::Sym>("DEC31/corresp.xml", "sym-corresp.ply");
  tri.Triangulate<vc::TriMethod::Unsym>("DEC31/corresp.xml", "unsym-corresp.ply");
}
