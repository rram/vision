// Sriramajayam

#include "Chessboard.h"
#include <iostream>

namespace vc
{
  // Construction through a file
  void Chessboard::Create(const cv::String filename)
  {
    std::cout<<"\nCreating chessboard from file "<<filename; std::fflush( stdout );
    
    // Open this file
    cv::FileStorage fs(filename, cv::FileStorage::READ);
    CV_Assert(fs.isOpened() && "\nChessboard::Create- Could not open file for construction.\n");
    
    auto& im = _images;
    
    auto fn = fs["image_count"];
    CV_Assert(!fn.empty() &&  "\nCamera::Create()- Could not read field image_count.\n");
    im._nImages = static_cast<unsigned int>(static_cast<int>(fs["image_count"]));
    //const int nImages = static_cast<int>(im._nImages);
    
    fn = fs["image_width"];
    CV_Assert(!fn.empty() &&  "\nCamera::Create()- Could not read field image_width.\n");
    cv::read(fn, im._imageSize.width, 0);
    
    fn = fs["image_height"];
    CV_Assert(!fn.empty() &&  "\nCamera::Create()- Could not read field image_height.\n");
    cv::read(fn, im._imageSize.height, 0);
    
    fn = fs["board_width"];
    CV_Assert(!fn.empty() &&  "\nCamera::Create()- Could not read field board_width.\n");
    cv::read(fn, _boardSize.width, 0);
    
    fn = fs["board_height"];
    CV_Assert(!fn.empty() &&  "\nCamera::Create()- Could not read field board_height.\n");
    cv::read(fn, _boardSize.height, 0);
    
    fn = fs["square_size"];
    CV_Assert(!fn.empty() && "\nCamera::Create()- Could not read field square_size\n");
    cv::read(fn, _squareSize, 0.);

    fn = fs["calib_images"];
    CV_Assert(!fn.empty() && "\nCamera::Create()- Could not read field calib_images\n");
    cv::read(fn, im._fileNames);
    CV_Assert(im._fileNames.size()==im._nImages && "\nCamera::Create()- inconsistent number of images.\n");
    
    fs.release();

    // done
  }


  // Write chessboard details to file
  void Chessboard::Save(const cv::String xmlfile) const
  {
    std::cout<<"\nSaving chessboard to file "<<xmlfile; std::fflush( stdout );
    
    cv::FileStorage fs(xmlfile, cv::FileStorage::WRITE);
    CV_Assert(fs.isOpened() && "\nChessboard::Save()- Could not open file for writing.\n");
    
    const auto& im = _images;
    fs<<"image_count"<<static_cast<int>(im._nImages);
    fs<<"image_width"<<im._imageSize.width;
    fs<<"image_height"<<im._imageSize.height;
    fs<<"board_width"<<_boardSize.width;
    fs<<"board_height"<<_boardSize.height;
    fs<<"square_size"<<_squareSize;
    fs<<"calib_images"<<_images._fileNames;
	
    fs.release();
  }
  
}
