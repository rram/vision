// Sriramajayam

#ifndef _CHESS_BOARD_H
#define _CHESS_BOARD_H

#include <vector>
#include <opencv2/core.hpp>

namespace vc
{
  struct ImageSet
  {
    std::vector<cv::String> _fileNames;
    unsigned int _nImages;
    cv::Size _imageSize;
  };

  
  class Chessboard
  {
  public:
    //! Default constructor
    inline Chessboard()
      :_boardSize(0,0), _squareSize(0)
      {
	_images._fileNames.clear();
	_images._nImages = 0;
	_images._imageSize = cv::Size(0,0);
      }
    
    //! Construction through a file
  Chessboard(const cv::String xmlfile)
      :Chessboard()
      { Create(xmlfile); }
    
    //! Copy constructor
    inline Chessboard(const Chessboard& obj):
      _boardSize(obj._boardSize),
      _squareSize(obj._squareSize),
      _images(obj._images) {}

    //! Read chessboard details from file
    void Create(const cv::String xmlfile);
    
    //! Write chessboard details to file
    void Save(const cv::String xmlfile) const;
    
    // Members
    cv::Size _boardSize; // Size of the board
    double _squareSize; // Size of each square
    ImageSet _images; // filenames with images of this chessboard
  };
  
}

#endif
