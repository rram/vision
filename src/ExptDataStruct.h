// Sriramajayam

#ifndef VC_EXPT_DATA_STRUCT
#define VC_EXPT_DATA_STRUCT

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <experimental/filesystem>

namespace vc
{
  namespace fsys = std::experimental::filesystem;
  
  struct ExptDataStruct
    {
      // Read xml file information
      ExptDataStruct(const cv::String xmlfile);

       // Create folder structures and copy files for very ordered experimental data
      void Organize(const cv::String recfile);

      // Data recorded from XML file
      cv::String lcamDir, rcamDir; // Absolute paths to l/r camera folders
      cv::String lcamCalib, rcamCalib; // Absolute paths to calibration files for l/r cameras
      cv::String lcamCorners, rcamCorners; // Absolute paths to corners files for l/r cameras
      cv::String workDir;   // Absolute path to working directory
      int nPoses; // Number of poses
      int nStages; // Number of stages per pose
      std::vector<int> nLevels; // Number of levels in each stage
      cv::String imgExtension; // What file extension to use
      cv::String imgStem; // non-numeric part of image name
      cv::Size imgSize; // Image sizes
	
      // File/Folder paths
    private:
      fsys::path lcamPath, rcamPath; // Paths to l/r camera data
      fsys::path workPath; // Path to working directory
      std::vector<fsys::path> posePaths; // Absolute path to each pose
      std::vector<fsys::path> stagePaths; // Relative path for each stage
      std::vector<fsys::path> lcamImagePaths, rcamImagePaths; // Absolute paths of images in l/r cameras, ordered by time stamp
      std::vector<fsys::path> lworkImagePaths, rworkImagePaths; // Absolute paths for images in working directories, 1-1 with l/r-camImagePaths.
      
      // Read input xml file for members
      void readXML(const cv::String xmlfile);
      
      // Create a template xml file
      void createTemplate(const cv::String xmlfile) const;
      
      // Create the set of all paths (sources and destinations)
      void populatePaths();

      // Create the required folder structures
      void createFolders() const;

      // Copy files from source to destination
      void copyFiles() const;

      // Print required xml files into workPath/inxml
      void createXMLs();
      
      // Record information about time stamps, organization
      void Record(const cv::String recfile) const;

      // Helper functions
      // Order images in a given camera with expected extension by time stamps
      void orderCameraImages(const fsys::path camPath, 
			     std::vector<fsys::path>& orderedImagePath) const;
      
      // Assign new paths for each camera image
      void assignWorkImagePaths(const cv::String camname,
				std::vector<fsys::path>& workPaths) const;

      // Copying images
      void copyImages(const std::vector<fsys::path>& camPaths,
		      const std::vector<fsys::path>& workPaths) const;
    };

}

#endif
