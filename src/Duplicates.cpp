// Sriramajayam

#include <Utils.h>
#include <experimental/filesystem>
#include <utility>
#include <map>
#include <iostream>

namespace vc
{
  namespace fsys = std::experimental::filesystem;
  
  // Anonymous namespace for helper functions
  namespace{

    // Sorted paths
    void sortFiles(const cv::String folder, const cv::String stem,
		   const cv::String extension,
		   std::vector<fsys::path>& orderedlist)
    {
      orderedlist.clear();
      
      // Read files in the camera directory and order them by an enumeration
      auto folderpath = folder.c_str();
      std::map<unsigned int, fsys::path> imgtimes;
      for(auto& img: fsys::directory_iterator(folderpath))
	{
	  fsys::path imgPath = static_cast<fsys::path>(img);

	  // Check this extension. Proceed only if it an expected extension
	  if(imgPath.extension()==fsys::path(extension.c_str()))
	    {
	      // Check that this is a regular file
	      CV_Assert(fsys::is_regular_file(img) && "\nUtils::IdentifyDuplicates()- image file is not a regular file.\n");

	      // Get this filename without extension
	      cv::String filename = cv::String(imgPath.filename().stem().c_str());
	      
	      // This filename is of the form stemXXXXX, where XXXXX are numerals
	      // Extract the numeral
	      unsigned int numeral = std::stoul(filename.substr(stem.size()));
	      CV_Assert(imgtimes.find(numeral)==imgtimes.end() && "Utils::IdentifyDuplicates()- Fould files with identical numeral.");
	      imgtimes[numeral] = imgPath;
	    }
	}
      
      orderedlist.clear();
      for(auto& img:imgtimes)
	orderedlist.push_back( img.second );

      return;
    }
  }
  

  // Identify duplicate images in a folder
  std::vector<std::pair<cv::String, cv::String>>
  IdentifyDuplicateImages(const cv::String folder,
			  const cv::String stem,
			  const cv::String extension,
			  const double TOL)
  {
    fsys::path folderpath = folder.c_str();
    
    // Check that this folder exists
    CV_Assert(fsys::exists(folderpath) && "Utils::IdentifyDuplicateImages()- folder path does not exist.");
    CV_Assert(fsys::is_directory(folderpath) && "Utils::IdentifyDuplicateImages()- path provided is not a directory.");
      
    // Get an ordered list of file names
    std::vector<fsys::path> orderedPaths;
    sortFiles(folder, stem, extension, orderedPaths);
    
    // Pairs of possibe duplicates
    std::vector<std::pair<cv::String, cv::String>> idPairs;
    
    // Loop over successive files
    const int nFiles = static_cast<int>(orderedPaths.size());
    CV_Assert(nFiles>=1 && "Utils::IdentifyDuplicateImages()- Folder has no files with provided extension.");
    std::cout<<"\nExamining successively identical pairs among "<<nFiles<<" files "; std::fflush( stdout );
    for(int f=0; f<nFiles-1; ++f)
      {
	//std::cout<<"\nComparing files "<<orderedPaths[f]<<" and "<<orderedPaths[f+1]; std::fflush( stdout );
	// Load successive images
	cv::Mat img1 = cv::imread(orderedPaths[f].c_str(), cv::IMREAD_GRAYSCALE);
	CV_Assert(!img1.empty() && "Utils::IdentifyDuplicateImages()- encountered an empty image.");
	cv::Mat img2 = cv::imread(orderedPaths[f+1].c_str(), cv::IMREAD_GRAYSCALE);
	CV_Assert(!img2.empty() && "Utils::IdentifyDuplicateImages()- encountered an empty image.");

	double nfactor = std::sqrt( cv::norm(img1, cv::NORM_L2)*cv::norm(img1, cv::NORM_L2) );
	// Compute the maximum difference
	double maxdiff = cv::norm(img1, img2, cv::NORM_L2)/nfactor;
	if(maxdiff<static_cast<double>(TOL))
	  // These images may be identical
	  idPairs.push_back( std::pair<cv::String, cv::String>(orderedPaths[f].c_str(), orderedPaths[f+1].c_str()) );
      }
    std::cout<<"\nFound "<<idPairs.size() <<" pair of successively-identical files."; std::fflush( stdout );
    return idPairs;
  }

    /*
    // Interactive deletion of duplicate files
    void InteractiveDelete(const std::vector<std::pair<cv::String, cv::String>>& duplicates)
    {
      std::cout<<"\nPRESS 0 to skip, 1 to delete image 1 and 2 to delete image 2"; std::fflush( stdout );
      cv::namedWindow("img1", CV_WINDOW_AUTOSIZE);
      cv::namedWindow("img2", CV_WINDOW_AUTOSIZE);
      for(auto& pairs:duplicates)
	{
	  cv::Mat img1 = cv::imread(pairs.first, cv::IMREAD_GRAYSCALE);
	  cv::Mat img2 = cv::imread(pairs.second, cv::IMREAD_GRAYSCALE);
	  if(!img1.empty() && !img2.empty())
	    {
	      cv::imshow("img1", img1);
	      cv::imshow("img2", img2);
	      int val=3;
	      std::cout<<"\nEnter choice 0/1/2: "; std::fflush( stdout );
	      while(val!=0 && val!=1 && val!=2)
		{
		  std::cin>>val;
		  if(val!=0 && val!=1 && val!=2)
		    {
		      std::cout<<"\nInvalid choice. Enter choice 0/1/2: ";
		      std::fflush( stdout );
		    }
		}
	      if(val!=0)
		{
		  fsys::path imgpath = (val==1) ? pairs.first.c_str() : pairs.second.c_str();
		  std::cout<<"\nDeleting file "<<imgpath; std::fflush( stdout );
		  fsys::remove(imgpath);
		}
	    }
	}
      cv::destroyWindow("img1");
      cv::destroyWindow("img2");
      }*/
  
}
