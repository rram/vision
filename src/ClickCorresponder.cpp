// Sriramajayam

#include <ClickCorresponder.h>
#include <random>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

namespace vc
{ 
  // Constructor
  ClickCorresponder::
  ClickCorresponder(const cv::String limage1, const cv::String rimage1,
		    const cv::String limage2, const cv::String rimage2)
    :L1image(limage1), R1image(rimage1),
     L2image(limage2), R2image(rimage2)
  {
    // Load all four images
    cv::Mat limg1 = cv::imread(limage1, CV_LOAD_IMAGE_GRAYSCALE);
    cv::Mat rimg1 = cv::imread(rimage1, CV_LOAD_IMAGE_GRAYSCALE);
    cv::Mat limg2 = cv::imread(limage2, CV_LOAD_IMAGE_GRAYSCALE);
    cv::Mat rimg2 = cv::imread(rimage2, CV_LOAD_IMAGE_GRAYSCALE);
    CV_Assert( (!limg1.empty() && !limg2.empty() &&
		!rimg1.empty() && !rimg2.empty() ) &&
	       "\nClickCorresponder- File not found.\n");

    // Set image references for each set of pixels
    L1.img = &limg1;
    L2.img = &limg2;
    R1.img = &rimg1;
    R2.img = &rimg2;
    
    // Create windows for each of the four image
    L1.winName = "lcam scene 1";
    R1.winName = "rcam scene 1";
    L2.winName = "lcam scene 2";
    R2.winName = "rcam scene 2";
    cv::namedWindow(L1.winName, CV_WINDOW_NORMAL);
    cv::namedWindow(L2.winName, CV_WINDOW_NORMAL);
    cv::namedWindow(R1.winName, CV_WINDOW_NORMAL);
    cv::namedWindow(R2.winName, CV_WINDOW_NORMAL);

    // Set mouse call backs for each window
    cv::setMouseCallback(L1.winName, onMouse, &L1);
    cv::setMouseCallback(L2.winName, onMouse, &L2);
    cv::setMouseCallback(R1.winName, onMouse, &R1);
    cv::setMouseCallback(R2.winName, onMouse, &R2);

    // Collect clicks for each set of pixels
    cv::imshow(L1.winName, *L1.img);
    cv::imshow(L2.winName, *L2.img);
    cv::imshow(R1.winName, *R1.img);
    cv::imshow(R2.winName, *R2.img);
    cv::waitKey(0);

    // Destroy windows
    cv::destroyWindow(L1.winName);
    cv::destroyWindow(L2.winName);
    cv::destroyWindow(R1.winName);
    cv::destroyWindow(R2.winName);

    // Check that all images have at least one pixel picked
    CV_Assert((L1.pts.size()>0 && L2.pts.size()>0 && R1.pts.size()>0 && R2.pts.size()>0) &&
	      "\nClickCorresponder: No pixels were picked in one or more images.\n");

    // Postprocess the last pixel
    L1.PostProcess();
    L2.PostProcess();
    R1.PostProcess();
    R2.PostProcess();

    std::cout<<"\nNumber of pixels picked: "
	     <<"\nLeft camera scene 1: "<<L1.pts.size()
      	     <<"\nRight camera scene 1: "<<R1.pts.size()
      	     <<"\nLeft camera scene 2: "<<L2.pts.size()
      	     <<"\nRight camera scene 2: "<<R2.pts.size()
	     <<"\n";
    std::fflush( stdout );

    // Sanity checks on number of pixels recorded
    const auto npts = L1.pts.size();
    CV_Assert((npts==L2.pts.size() &&
	       npts==R1.pts.size() &&
	       npts==R2.pts.size()) &&
	      "\nClickCorresponder::Inconsistent number of points.\n");

    std::cout<<"\nClickCorresponder: completed point registration.\n";
    std::fflush( stdout );

    // Remove image references
    L1.img = nullptr;
    L2.img = nullptr;
    R1.img = nullptr;
    R2.img = nullptr;
  }
  

  // Displays calculated correspondence
  void ClickCorresponder::
  View(const cv::String limage1, const cv::String rimage1,
       const cv::String limage2, const cv::String rimage2) const
  {
    // Load all four images
    cv::Mat limg1 = cv::imread(L1image, CV_LOAD_IMAGE_COLOR);
    cv::Mat rimg1 = cv::imread(R1image, CV_LOAD_IMAGE_COLOR);
    cv::Mat limg2 = cv::imread(L2image, CV_LOAD_IMAGE_COLOR);
    cv::Mat rimg2 = cv::imread(R2image, CV_LOAD_IMAGE_COLOR);
    CV_Assert( (!limg1.empty() && !limg2.empty() &&
		!rimg1.empty() && !rimg2.empty() ) &&
	       "\nClickCorresponder- File not found.\n");

    // Number of pixels to plot
    const int npts = L1.pts.size();
    
    // Plot each pixel with a different color
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist(1, 255);
    cv::Vec3b color;
    cv::Point pt;
    for(int p=0; p<npts; ++p)
      {
	// Generate a random color for this point
	color.val[0] = dist(gen);
	color.val[1] = dist(gen);
	color.val[2] = dist(gen);

	// Assign this color to the pixel in each image
	cv::circle(limg1, cv::Point(L1.pts[p].xval, L1.pts[p].yval), 5, color);
	cv::circle(limg2, cv::Point(L2.pts[p].xval, L2.pts[p].yval), 5, color);
	cv::circle(rimg1, cv::Point(R1.pts[p].xval, R1.pts[p].yval), 5, color);
	cv::circle(rimg2, cv::Point(R2.pts[p].xval, R2.pts[p].yval), 5, color);
      }

    // Plot the four images
    CvMat cvimg = limg1;
    cvSaveImage(limage1.c_str(), &cvimg);
    cvimg = limg2;
    cvSaveImage(limage2.c_str(), &cvimg);
    cvimg = rimg1;
    cvSaveImage(rimage1.c_str(), &cvimg);
    cvimg = rimg2;
    cvSaveImage(rimage2.c_str(), &cvimg);
    
    // Done.
    return;
  }
  
  // Mouse callback
  void ClickCorresponder::onMouse(int event,
				  int x, int y,
				  int flag, void* data)
  {
    auto* mypixels = static_cast<Pixels*>(data);
    auto& pixelnum = mypixels->pixelnum;
    auto& pts = mypixels->pts;
    if(static_cast<int>(pts.size())<=pixelnum)
      pts.resize(pixelnum+1);
    auto& pt = pts[pixelnum];
    
    // Register clicks for this pixel
    if(event==cv::EVENT_LBUTTONDOWN)
      {
	//std::cout<<"\nSensed click at "<<x<<", "<<y;
	//std::fflush( stdout );
	++pt.nClicks;
	pt.xval += static_cast<double>(x);
	pt.yval += static_cast<double>(y);
      }
  else if(event==cv::EVENT_RBUTTONDOWN)
    {
      // This pixel is done. Record for the next pixel
      pt.xval /= static_cast<double>(pt.nClicks);
      pt.yval /= static_cast<double>(pt.nClicks);
      pt.flag = true;
      ++pixelnum;

      // Display the identified click as a circle
      cv::circle(*mypixels->img, cv::Point(pt.xval, pt.yval), 4, cv::Scalar(127));
      cv::imshow(mypixels->winName, *mypixels->img);
      //std::cout<<"\nUpdating pixel number.\n";
      //std::fflush( stdout );
    }
  }

  
  // Saves the calculated correspondence to a file
  void ClickCorresponder::Save(const cv::String file1,
			       const cv::String file2) const
  {
    // Save the details of pixels from scene 1 to file 1
    // and from scene 2 to file 2.
    std::fstream p1file, p2file;
    p1file.open(file1, std::ios::out);
    p2file.open(file2, std::ios::out);
    CV_Assert((p1file.good() && p2file.good()) && "\nClickCorresponder::Save- Could not open file.\n");
    const unsigned int npts = L1.pts.size();
    for(unsigned int p=0; p<npts; ++p)
      {
	p1file<<L1.pts[p].xval<<" "<<L1.pts[p].yval<<" "<<R1.pts[p].xval<<" "<<R1.pts[p].yval<<"\n";
	p2file<<L2.pts[p].xval<<" "<<L2.pts[p].yval<<" "<<R2.pts[p].xval<<" "<<R2.pts[p].yval<<"\n";
      }
    p1file.close();
    p2file.close();
    std::cout<<"\nClickCorresponder::Save(): Saved scene 1 pixels to "<<file1
	     <<" and scene 2 pixels to "<<file2;
    std::fflush( stdout );
  }


  // Post process last pixel in each image
  void ClickCorresponder::Pixels::PostProcess()
  {
    CV_Assert(pts.size()>0 && "\nClickCorresponder::No pixels found.\n");
    
    // Process the last pixel data
    auto& lastpt = pts[pts.size()-1];
    if(lastpt.flag==false)
      {
	if(lastpt.nClicks==0)
	  // This pixel is a dummy. Erase it
	  pts.resize(pts.size()-1);
	else
	  {
	    lastpt.xval /= static_cast<double>(lastpt.nClicks);
	    lastpt.yval /= static_cast<double>(lastpt.nClicks);
	    lastpt.flag = true;
	  }
      }
    return;
  }
  
}
