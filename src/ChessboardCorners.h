// Sriramajayam

#ifndef _CHESSBOARD_CORNERS_H
#define _CHESSBOARD_CORNERS_H

#include <vector>
#include <opencv2/core.hpp>

namespace vc
{
  class ChessboardCorners
  {
  public:
    
    //! Default constructor
    inline ChessboardCorners()
      :_nImages(0), _cornersIn3D({}),
      _cornersInImages({}), _rvecs({}), _tvecs({}) {}

    //! Construction from a file
    inline ChessboardCorners(const cv::String xmlfile)
      :ChessboardCorners()
    { Create(xmlfile); }

    //! Copy constructor
    inline ChessboardCorners(const ChessboardCorners& Obj):
      _nImages(Obj._nImages),
      _cornersIn3D(Obj._cornersIn3D),
      _cornersInImages(Obj._cornersInImages),
      _rvecs(Obj._rvecs),
      _tvecs(Obj._tvecs) {}
    
    // Save data about corners to file
    void Save(const cv::String xmlfile) const;

    // Read data from file
    void Create(const cv::String xmlfile);

    // Members
    unsigned int _nImages;
    
    // Input coordinates of corners
    std::vector<std::vector<cv::Point3f>> _cornersIn3D;
    
    // Coordinates of corners in each image.
    std::vector<std::vector<cv::Point2f>> _cornersInImages;
    
    // Rotation and translations of corners
    std::vector<cv::Mat> _rvecs, _tvecs;
  };

}

#endif
