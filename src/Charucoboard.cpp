// Sriramajayam

#include <Charucoboard.h>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <string>
#include <iostream>
#include <random>
#include <algorithm>
#include <set>
#include <array>
#include <map>

namespace vc
{
  const std::map<cv::String, cv::aruco::PREDEFINED_DICTIONARY_NAME>
  Charucoboard::_dictionaryMap
  = {{"DICT_4X4_100", cv::aruco::DICT_4X4_100},
     {"DICT_4X4_250", cv::aruco::DICT_4X4_250},
     {"DICT_4X4_1000", cv::aruco::DICT_4X4_1000},
     {"DICT_5X5_50", cv::aruco:: DICT_5X5_50},
     {"DICT_5X5_100", cv::aruco:: DICT_5X5_100},
     {"DICT_5X5_250", cv::aruco:: DICT_5X5_250},
     {"DICT_5X5_1000", cv::aruco:: DICT_5X5_1000}, 
     {"DICT_6X6_50", cv::aruco::DICT_6X6_50},  
     {"DICT_6X6_100", cv::aruco::DICT_6X6_100},  
     {"DICT_6X6_250", cv::aruco::DICT_6X6_250},
     {"DICT_6X6_1000", cv::aruco::DICT_6X6_1000},
     {"DICT_7X7_50", cv::aruco::DICT_7X7_50},
     {"DICT_7X7_100", cv::aruco::DICT_7X7_100},
     {"DICT_7X7_250", cv::aruco::DICT_7X7_250},
     {"DICT_7X7_1000", cv::aruco::DICT_7X7_1000}};
  
  // Create board from parameters specified in a file
  void Charucoboard::Create(const cv::String xmlfile)
  {
    std::cout<<"\nCreating Charucoboard from file "<<xmlfile;
    std::fflush( stdout );

    // Open this file
    cv::FileStorage fs(xmlfile, cv::FileStorage::READ);
    CV_Assert(fs.isOpened() &&
	      "\nCharucoboard::Create()- Could not open file for construction.\n");

    // Read the elements
    auto fn = fs["squaresX"];
    CV_Assert(!fn.empty() &&
	      "\nCharucoboard::Create()- Could not read squaresX.\n");
    int squaresX = 0;
    cv::read(fn, squaresX, 0);

    fn = fs["squaresY"];
    CV_Assert(!fn.empty() &&
	      "\nCharucoboard::Create()- Could not read squaresY.\n");
    int squaresY = 0;
    cv::read(fn, squaresY, 0);

    fn = fs["squareLength"];
    CV_Assert(!fn.empty() &&
	      "\nCharucoboard::Create()- Could not read squareLength.\n");
    float squareLength = 0;
    cv::read(fn, squareLength, 0);
    
    fn = fs["markerLength"];
    CV_Assert(!fn.empty() &&
	      "\nCharucoboard::Create()- Could not read markerLength.\n");
    float markerLength = 0;
    cv::read(fn, markerLength, 0);

    fn = fs["markerBits"];
    CV_Assert(!fn.empty() &&
	      "\nCharucoboard::Create()- Could not read markerBits.\n");
    int markerBits;
    cv::read(fn, markerBits, 0);
    CV_Assert((markerBits==4 || markerBits==5 || markerBits==6 || markerBits==7) &&
	      "\nCharucoboard::Create()- markerBits should be 4, 5, 6 or 7.\n");

    // Compute the minimum number of markers required based on the size of the chessboard
    int nMinMarkers = (squaresX*squaresY)/2;
    nMinMarkers += 2; // Accounts for the odd case, safety factor of 1.

    // Has the number of markers been specified?
    int numMarkers = 0;
    fn = fs["numMarkers"];
    if(!fn.empty())
      {
	cv::read(fn, numMarkers, 0);
	CV_Assert((numMarkers==50 || numMarkers==100 ||
		   numMarkers==250 || numMarkers==1000) &&
		  "\nCharucoboard::Create()- numMarkers can be 50, 100, 250 or 1000.\n");
	CV_Assert(numMarkers>=nMinMarkers &&
		  "\nCharucoBoard::Create()- numMarkers is smaller than min. required.\n");
      }
    else
      {
	// Select the appropriate board
	if(nMinMarkers<50)
	  numMarkers = 50;
	else if(nMinMarkers<100)
	  numMarkers = 100;
	else if(nMinMarkers<250)
	  numMarkers = 250;
	else if(nMinMarkers<1000)
	  numMarkers = 1000;
	else
	  CV_Assert(false && "\nCharucoboard::Create()- number of markers required exceeds 1000.\n");
      }

    
    // Check options read from the file
    CV_Assert(squaresX>0 && "\nCharucoboard::Create()- unexpected value for squaresX");
    CV_Assert(squaresY>0 && "\nCharucoboard::Create()- unexpected value for squaresY");
    CV_Assert(squareLength>0 &&
	      "\nCharucoboard::Create()- unexpected value for squareLength");
    CV_Assert(markerLength>0 &&
	      "\nCharucoboard::Create()- unexpected value for markerLength");
    CV_Assert(squareLength>markerLength &&
	      "\nCharucoboard::Create()- square length should be larger than marker length.\n");
    CV_Assert(markerBits<markerLength &&
	      "\nCharucoboard::Create()- markerLength should exceed markerBits");
    
    // Create a dictionary name
    cv::String dictName =
      cv::String("DICT_") +
      cv::String(std::to_string(markerBits)) +
      cv::String("X") +
      cv::String(std::to_string(markerBits)) +
      cv::String("_")+
      cv::String(std::to_string(numMarkers));
    
    // Initialize the dictionary
    std::cout<<"\nChoosing dictionary "<<dictName<<" to create Charucoboard.";
    auto it = _dictionaryMap.find(dictName);
    CV_Assert(it!=_dictionaryMap.end() &&
	      "\nCharucoboard::Create()- unexpected dictionary name encountered.\n");
    auto dictionary = cv::aruco::getPredefinedDictionary(it->second);

    // Check dictionary
    CV_Assert(markerBits==dictionary->markerSize &&
	      "\nCharucoboard::Create()- incorrect number of bits in dictionary.\n");
    CV_Assert(numMarkers==dictionary->bytesList.rows &&
	      "\nCharucoboard::Create()- incorrect number of markers in dictionary.\n");

    // Create the board
    _board = cv::aruco::CharucoBoard::create(squaresX, squaresY, squareLength,
					     markerLength, dictionary);
  }


  // Draw the board
  void Charucoboard::Draw(const cv::String filename,
			  const int marginSize, const int borderBits)
  {
    // Chessboard size
    auto cbSize = _board->getChessboardSize();
    auto squareLength = _board->getSquareLength();
    
    // Compute the required image size
    cv::Size imgSize(cbSize.width*squareLength+2*marginSize,
		     cbSize.height*squareLength+2*marginSize);

    // Draw
    cv::Mat img;
    _board->draw(imgSize, img, marginSize, borderBits);

    // Print the image
    CvMat cvimg = img;
    std::cout<<"\nSaving charucoboard image to file "<<filename;
    std::fflush( stdout );
    cvSaveImage(filename.c_str(), &cvimg);
  }


  // Save a board to as an xml file
  void Charucoboard::Save(const cv::String xmlfile) const
  {
    std::cout<<"\nSaving charucoboard to file "<<xmlfile;
    std::fflush( stdout );
    
    // Open this file
    cv::FileStorage fs(xmlfile, cv::FileStorage::WRITE);
    CV_Assert(fs.isOpened() &&
	      "\nCharucoboard::Save()- Could not open file for construction.\n");

    // Chessboard size
    auto cbSize = _board->getChessboardSize();
    cvWriteComment(*fs, (char*)"Number of squares in the X direction", 0);
    fs << "squaresX"<<cbSize.width;

    cvWriteComment(*fs, (char*)"Number of squares in the Y direction", 0);
    fs << "squaresY"<<cbSize.height;

    // Square and marker length
    cvWriteComment(*fs, (char*)"Length of each square (in pixels)", 0);
    fs << "squareLength" << _board->getSquareLength();
    cvWriteComment(*fs, (char*)"Length of each marker (in pixels, smaller than squareLength", 0);
    fs << "markerLength" << _board->getMarkerLength();

    // Dictionary details
    cvWriteComment(*fs, (char*)"Number of marker bits desired: 4,5,6 or 7 only", 0);
    fs << "markerBits" << _board->dictionary->markerSize;
    cvWriteComment(*fs, (char*)"Optional: Number of markers in dictionary. If not specified, a minimum number will be calculated. Rounded up to 50, 100, 250 or 1000", 0);
    fs << "numMarkers" << _board->dictionary->bytesList.rows;
    fs.release();
  }

  // Main functionality: detect corners in an image
  void Charucoboard::Detect(CharucoBoardImage& cbImage) const
  {
    CV_Assert(!cbImage.imgName.empty() &&
	      "\nCharucoBoard::Detect()- Image name not provided.\n");
    
    // Read this image
    cv::Mat img = cv::imread(cbImage.imgName, CV_LOAD_IMAGE_GRAYSCALE);
    CV_Assert(!img.empty() && "\nCharucoBoard::Detect()- Could not open image.\n");

    auto& nMarkers = cbImage.nMarkers;
    auto& nCorners = cbImage.nCorners;
    auto& markerIDs = cbImage.markerIDs;
    auto& markerCorners = cbImage.markerCorners;
    auto& charucoIDs = cbImage.charucoIDs;
    auto& charucoCorners = cbImage.charucoCorners;

    // Detect markers
    markerIDs.clear();
    markerCorners.clear();
    cv::aruco::detectMarkers(img, _board->dictionary, markerCorners, markerIDs);

    // Sanity check on number of markers
    nMarkers = static_cast<int>(markerIDs.size());
    CV_Assert(static_cast<int>(markerCorners.size())==nMarkers &&
	      "\nCharucoBoard::Detect()- Unexpected number of markers found.\n");
    CV_Assert(nMarkers>0 && "\nCharucoBoard::Detect()- No markers found.\n");
    CV_Assert(nMarkers<=static_cast<int>(_board->ids.size()) &&
	      "\nCharucoBoard::Detect()- Found too many markers.\n");

    // Find corners
    charucoIDs.release();
    charucoCorners.release();
    nCorners = cv::aruco::interpolateCornersCharuco
      (markerCorners, markerIDs, img, _board, charucoCorners, charucoIDs);
    CV_Assert(nCorners>0 &&
	      "\nCharucoBoard::Detect()- Did not find any corners.\n");
    CV_Assert((charucoIDs.rows==nCorners && charucoCorners.rows==nCorners) &&
	      "\nCharucoBoard::Detect()- Inconsistent number of corners found.\n");
    auto cbsize = _board->getChessboardSize();
    CV_Assert(nCorners<=(cbsize.width-1)*(cbsize.height-1) &&
	      "\nCharucoBoard::Detect()- Found too many corners.\n");

    // Visualize identification
    cv::cvtColor(img, img, CV_GRAY2BGR);
    cv::Vec3b blue(255,0,0);
    cv::Vec3b red(0,0,255);

    // Markers in blue
    for(int m=0; m<nMarkers; ++m)
      for(int j=0; j<4; ++j)
	cv::circle(img, cv::Point(markerCorners[m][j].x, markerCorners[m][j].y),
		   5, blue, CV_FILLED);

    // Corners in red
    cv::aruco::drawDetectedCornersCharuco(img, charucoCorners, charucoIDs, red);

    // Save visualization
    auto pos = cbImage.imgName.find_last_of((char*)"/");
    cv::String filename = cv::String("detected_") + cv::String(cbImage.imgName, pos+1);
    std::cout<<"\nVisualization of detected corners in "<<cbImage.imgName
	     <<" saved in "<<filename; std::fflush( stdout );
			     CvMat cvimg = img;
			     cvSaveImage(filename.c_str(), &cvimg);
  }

    
    // Compute corresponding corners in a pair of images
    void Charucoboard::Match(const cv::String img1Name, const cv::String img2Name,
			     cv::String ptsfile) const
    {
      CharucoBoardImage cb1, cb2;
      Detect(img1Name, cb1);
      Detect(img2Name, cb2);

      // Convert corner IDs from Mat to std::vector
      std::vector<int> id1(cb1.nCorners);
      for(int i=0; i<cb1.nCorners; ++i) id1[i] = cb1.charucoIDs.at<int>(i,0);
      std::vector<int> id2(cb2.nCorners);
      for(int i=0; i<cb2.nCorners; ++i) id2[i] = cb2.charucoIDs.at<int>(i,0);
      
      // Corners with matching indices in both images
      std::vector<int> matchCornerIDs;
      std::set_intersection(id1.begin(), id1.end(), id2.begin(), id2.end(),
			    std::back_inserter(matchCornerIDs));
      const int nMatchCorners = static_cast<int>(matchCornerIDs.size());
      CV_Assert(nMatchCorners>0 &&
		"\nCharucoboard::Match()- no matching corners found.\n");
      std::cout<<"\nNumber of matched corners: "<<nMatchCorners; std::fflush( stdout );
      
      // Invert map from index to IDs.
      std::map<int, int> inv_id1, inv_id2;
      for(int i=0; i<cb1.nCorners; ++i) inv_id1[id1[i]] = i;
      for(int i=0; i<cb2.nCorners; ++i) inv_id2[id2[i]] = i;

      // Look up coordinates of corners identified in both images
      cv::Mat_<double> leftCorresp(nMatchCorners, 2);
      cv::Mat_<double> rightCorresp(nMatchCorners, 2);
      cv::Mat_<int> matchID(nMatchCorners, 1);
      for(int i=0; i<nMatchCorners; ++i)
	{
	  auto& id = matchCornerIDs[i];
	  auto it1 = inv_id1.find(id);
	  auto it2 = inv_id2.find(id);
	  CV_Assert((it1!=inv_id1.end() && it2!=inv_id2.end()) &&
		    "\nCharucoboard::Match()- inconsistent inverse index map.\n");

	  // Note down pixel coordinates from the two images
	  auto& corner1 = cb1.charucoCorners.at<cv::Point2f>(it1->second, 0);
	  leftCorresp(i,0) = corner1.x;
	  leftCorresp(i,1) = corner1.y;
	  
	  auto& corner2 = cb2.charucoCorners.at<cv::Point2f>(it2->second, 0);
	  rightCorresp(i,0) = corner2.x;
	  rightCorresp(i,1) = corner2.y;
	  
	  matchID(i,0) = id;
	}

      // Visualize the corresponding set of corners
      cv::Mat img1 = cv::imread(cb1.imgName, CV_LOAD_IMAGE_COLOR);
      cv::Mat img2 = cv::imread(cb2.imgName, CV_LOAD_IMAGE_COLOR);

      // Random color generation
      std::random_device rd;
      std::mt19937 gen(rd());
      std::uniform_int_distribution<> dist(1, 255);
      cv::Vec3b color;

      const int rad = 5;
      cv::Vec3b red(0,0,255);
      cv::Point2f pt;
      cv::Point2f offset(rad,rad);
      for(int i=0; i<nMatchCorners; ++i)
	{
	  // text to print
	  cv::String txt = cv::String("cnr ")+cv::String(std::to_string(matchID(i,0)).c_str());

	  // color of dot
	  color.val[0] = dist(gen);
	  color.val[1] = dist(gen);
	  color.val[2] = dist(gen);

	  // Image 1
	  pt.x = leftCorresp(i,0);
	  pt.y = leftCorresp(i,1);
	  cv::circle(img1, pt, rad, color, CV_FILLED);
	  putText(img1, txt, pt+offset, cv::FONT_HERSHEY_COMPLEX_SMALL, 1., red, 2);

	  // Image 2
	  pt.x = rightCorresp(i,0);
	  pt.y = rightCorresp(i,1);
	  cv::circle(img2, pt, rad, color, CV_FILLED);
	  putText(img2, txt, pt+offset, cv::FONT_HERSHEY_COMPLEX_SMALL, 1., red, 2);
	}
      
      auto pos = cb1.imgName.find_last_of((char*)"/");
      cv::String filename = "match_" + cv::String(cb1.imgName, pos+1);
      CvMat cvimg = img1;
      std::cout<<"\nMatched corners in image "<<cb1.imgName<<" visualized in "<<filename;
      cvSaveImage(filename.c_str(), &cvimg);

      pos = cb2.imgName.find_last_of((char*)"/");
      filename = "match_" + cv::String(cb2.imgName, pos+1);
      cvimg = img2;
      std::cout<<"\nMatched corners in image "<<cb2.imgName<<" visualized in "<<filename;
      cvSaveImage(filename.c_str(), &cvimg);

      // Save the identified pixels and IDs in an xml file
      cv::FileStorage fs(ptsfile, cv::FileStorage::WRITE);
      CV_Assert(fs.isOpened() && "\ncv::Charucoboard::Match()- Could not open pixels file for writing.\n");
      cvWriteComment(*fs, (char*)"Pixel set 1", 0);
      fs << "pixel_set_1" << leftCorresp;
      cvWriteComment(*fs, (char*)"Pixel set in scene 2", 0);
      fs << "pixel_set_2" << rightCorresp;
      cvWriteComment(*fs, (char*)"IDs of corners for matching pixel sets", 0);
      fs << "corner_ids" << matchID;
      fs.release();
      return;
    }


  // Anonymous namespace
  namespace
  {
    // Helper function to read pixels and corner ids
    void ReadCorners(const cv::String xmlfile,
		     cv::Mat_<double>& left, cv::Mat_<double>& right,
		     cv::Mat_<int>& ids)
    {
      // Read the sets of matching corners and their IDs from the pair of xml files
      cv::FileStorage fs(xmlfile, cv::FileStorage::READ);
      CV_Assert(fs.isOpened() && "\nCharucoboard::ReadCorners()- Could not open file for reading.\n");
      auto fn = fs["pixel_set_1"];
      CV_Assert(!fn.empty() && "\nCharucoboard::ReadCorners()- Could not read pixels for corners.\n");
      fn >> left;
      fn = fs["pixel_set_2"];
      CV_Assert(!fn.empty() && "\nCharucoboard::ReadCorners()- Could not read pixels for corners.\n");
      fn >> right;
      fn = fs["corner_ids"];
      CV_Assert(!fn.empty() && "\nCharucoboard::ReadCorners()- Could not read pixel ids for corners.\n");
      fn >> ids;
      fs.release();
    
      // Check dimensions
      CV_Assert((left.size()==right.size() && left.rows==ids.rows && ids.cols==1 && left.cols==2) &&
		"\nCharucoboard::ReadCorners()- unexpected pixel data format.\n");
      return;
    }

    // Identify common list of ids
    void IdentifyCommonCorners(const cv::Mat_<int>& A_ids, const cv::Mat_<int>& B_ids,
			       std::set<int>& commonIDs)
    {
      std::set<int> A_idset, B_idset;
      for(int i=0; i<A_ids.rows; ++i)
	A_idset.insert( A_ids(i,0) );
      for(int i=0; i<B_ids.rows; ++i)
	B_idset.insert( B_ids(i,0) );

      // Common ids
      commonIDs.clear();
      std::set_intersection(A_idset.begin(), A_idset.end(),
			    B_idset.begin(), B_idset.end(),
			    std::inserter(commonIDs, commonIDs.begin()));
      return;
    }


    // Compute 3D coordinates of corners with given set of IDs
    void ComputeCorners(const cv::Mat_<double>& left, const cv::Mat_<double>& right,
			const cv::Mat_<int>& ids, const std::set<int>& commonIDs,
			const Triangulator& tri, const TriMethod method,
			cv::Mat_<double>& cnrCoords)
    {
      // Inverse map from IDs to row # 
      std::map<int, int> id2rowMap;
      for(int row=0; row<ids.rows; ++row)
	id2rowMap[ids(row,0)] = row;

      // Which function to invoke for triangulation
      //std::function<emplate<TriMethod tm>
      //void Triangulate(const cv::Point2d& xl, const cv::Point2d& xr, cv::Point3d& X) const;
      
      // Loop over common ids.
      // Identify its pixel coordinates in the left and right images
      // Triangulate. Save the coordinates.
      cv::Point3d X;
      cv::Mat_<double> XMat(1,3);
      cv::Point2d leftpx, rightpx;
      for(auto id:commonIDs)
	{
	  auto it = id2rowMap.find(id);
	  CV_Assert(it!=id2rowMap.end());
	  auto& row = it->second;
	  leftpx.x = left(row, 0); leftpx.y = left(row, 1);
	  rightpx.x = right(row, 0); rightpx.y = right(row, 1);

	  // Triangulate
	  tri.Triangulate<TriMethod::Sym>(leftpx, rightpx, X);
	  XMat(0,0) = X.x; XMat(0,1) = X.y; XMat(0,2) = X.z;
	  cnrCoords.push_back(XMat);
	}
      return;
    }
    
  }


  // Identify overlapping ids
  // Compute coordinate transformation between a pair of matching points
  void Charucoboard::ComputeTransformation(const cv::String xmlfile1, const cv::String xmlfile2,
					   const Triangulator& tri,
					   const TriMethod method, const cv::String rtfile) const
  {
    // Read the sets of matching corners and their IDs from the pair of xml files
    cv::Mat_<double> A_left, A_right; // Corresponding corners in pose A
    cv::Mat_<int> A_ids; // Corresponding corner IDs in pose A
    ReadCorners(xmlfile1, A_left, A_right, A_ids);

    // Read the sets of matching corners and their IDs from the pair of xml files
    cv::Mat_<double> B_left, B_right; // Corresponding corners in pose B
    cv::Mat_<int> B_ids; // Corresponding corner IDs in pose B
    ReadCorners(xmlfile2, B_left, B_right, B_ids);

    // Get the set of common corner IDs
    std::set<int> commonIDs;
    IdentifyCommonCorners(A_ids, B_ids, commonIDs);
    const int nCommonIDs = static_cast<int>(commonIDs.size());
    CV_Assert(nCommonIDs>6 && "\nCharucoboard::ComputeTransformation()- Fewer than 6 common corners found between poses.\n");
    
    // Cartesian coordinates of common pixels in the two images from triangulation
    cv::Mat_<double> A_cnrCoords, B_cnrCoords;
    ComputeCorners(A_left, A_right, A_ids, commonIDs, tri, method, A_cnrCoords);
    ComputeCorners(B_left, B_right, B_ids, commonIDs, tri, method, B_cnrCoords);
    CV_Assert((A_cnrCoords.size()==B_cnrCoords.size() && B_cnrCoords.cols==3 && B_cnrCoords.rows==nCommonIDs)
	      && "\nCharucoboard::ComputeTransformation()- Unexpected number of common corners found.\n");
    
    // Compute the rigid body transformation taking B_cnrCoord to A_cnrCoords.
    // That is, R B_cnrCoords -> A_cnrCoords.

    // Centroid of each point set
    std::array<double, 3> A_centroid({0.,0.,0.}), B_centroid({0.,0.,0.});
    for(int i=0; i<nCommonIDs; ++i)
      for(int j=0; j<3; ++j)
	{
	  A_centroid[j] += A_cnrCoords(i,j);
	  B_centroid[j] += B_cnrCoords(i,j);
	}
    for(int k=0; k<3; ++k)
      {
	A_centroid[k] /= static_cast<double>(nCommonIDs);
	B_centroid[k] /= static_cast<double>(nCommonIDs);
      }
    
    // Compute the covariant matrix
    cv::Mat_<double> covMat(3,3);
    covMat = cv::Mat_<double>::zeros(3,3);
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	for(int p=0; p<nCommonIDs; ++p)
	  covMat(i,j) += (A_cnrCoords(p,i)-A_centroid[i])*(B_cnrCoords(p,j)-B_centroid[j]);

    // SVD of the covariance matrix
    cv::Mat_<double> sVals, Umat, Vtmat;
    cv::SVD::compute(covMat, sVals, Umat, Vtmat);
    double det = cv::determinant(Umat*Vtmat);
    cv::Mat_<double> resetVals = cv::Mat_<double>::eye(3,3);
    if(det<0.) resetVals(2,2) = -1.;
    cv::Mat_<double> RMat(3,3);
    RMat = Vtmat.t()*resetVals*Umat.t();
    CV_Assert(std::abs(cv::determinant(RMat)-1.)<1.e-6 &&
	      "\nCharucoboard::ComputeTransformation()- inconsistent determinant for rotation.\n");

    // Translation
    cv::Mat_<double> TMat(3,1);
    for(int i=0; i<3; ++i)
      {
	TMat(i,0) = B_centroid[i];
	for(int j=0; j<3; ++j)
	  TMat(i,0) -= RMat(i,j)*A_centroid[j];
      }

    // Save reconstructed points from the two poses, R, and T
    cv::FileStorage fs(rtfile, cv::FileStorage::WRITE);
    CV_Assert(fs.isOpened() && "\nCharucoboard::ComputeTransformation()- Could not open file to write.\n");
    cvWriteComment(*fs, (char*)"Corresponding corner from pose 1", 0);
    fs << "point_set_1" << A_cnrCoords;
    cvWriteComment(*fs, (char*)"Corresponding corner from pose 2", 0);
    fs << "point_set_2" << B_cnrCoords;
    cvWriteComment(*fs, (char*)"Rotation and translation mapping point set 2 onto point set 1.", 0);
    fs << "rot_mat" << RMat;
    fs << "trans_mat" <<TMat;
    fs.release();
    return;
  }

}
