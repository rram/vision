// Sriramajayam

#ifndef VISION_DATA_TYPES
#define VISION_DATA_TYPES

#include <utility>
#include <bitset>
#include <vector>
#include <array>

namespace vc
{
  // A pixel is a pair of x,y coordinates on a lattice
  using Pixel = std::pair<int, int>;

  // Distinguish left and right cameras
  enum class CamLabel {Left, Right};

  // Encoding a pixel with a sequence of bits
  template<int nStages, long unsigned int nLevels>
    using PixelBits = std::array<std::bitset<nLevels>, nStages>;

  // Representing an image as a 2D vector of PixelBits
  template<int nStages, long unsigned int nLevels>
    using ImageBits = std::vector<std::vector<PixelBits<nStages,nLevels>>>;

  // Boolean valued 2D vector
  using Boolean2D = std::vector<std::vector<bool>>;
}

#endif
