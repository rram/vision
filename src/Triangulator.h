// Sriramajayam

#ifndef _TRIANGULATOR_
#define _TRIANGULATOR_

#include <StereoRig.h>
#include <DataTypes.h>
#include <fstream>

namespace vc
{
  enum class TriMethod {Unsym, Sym, Sampson, Optimal};
  
  class Triangulator
  {
  public:

    // Constructor
    inline Triangulator(const StereoRig& rig)
      :_rig(rig)
    {
      double flag = cv::invert(_rig.GetLeftCamera().GetCameraMatrix(), _lKinv, cv::DECOMP_LU);
      CV_Assert(flag>0 && "\nTriangulator::Triangulator()- could not invert left cam matrix.\n");
      flag = cv::invert(_rig.GetRightCamera().GetCameraMatrix(), _rKinv, cv::DECOMP_LU);
      CV_Assert(flag>0 && "\nTriangulator::Triangulator()- could not invert right cam matrix.\n");
    }

    // Destructor, does nothing
    inline virtual ~Triangulator() {}

    // Copy constructor
    inline Triangulator(const Triangulator& obj)
      :_rig(obj._rig),
      _lKinv(obj._lKinv),
      _rKinv(obj._rKinv) {}

    // Returns the stereo rig
    inline const StereoRig& GetStereoRig() const
    { return _rig; }

    // Main functionality:
    // triangulates points with given pixel coordinates
    // according to one of requested methods
    // Specialized implementations to be provided.
    template<TriMethod tm>
      void Triangulate(const cv::Point2d& xl, const cv::Point2d& xr, cv::Point3d& X) const;

    // Main functionality:
    // triangulates points read from a file
    // with the specified method
    template<TriMethod tm>
      void Triangulate(const cv::String xmlfile, const cv::String cloudfile) const;
    
  private:
    const StereoRig& _rig; //!< Reference to stereo-rig
    cv::Mat _lKinv, _rKinv;  //!< Pre-compute the left and right camera matrix inverses
  };


  
  // Main functionality: triangulates points read from a file with the specified method
  template<TriMethod tm>
    void Triangulator::Triangulate(const cv::String xmlfile, const cv::String pointsfile) const
    {
      // Open the given xml file
      cv::FileStorage fs(xmlfile, cv::FileStorage::READ);
      CV_Assert(fs.isOpened() && "\nTriangulator::Triangulate()- Could not open file.\n");

      // read points
      cv::Mat lMat, rMat;
      auto fn = fs["pixel_set_1"];
      CV_Assert(!fn.empty() && "\nTriangulator::Triangulate()- could not read field pixel_set_1.\n");
      cv::read(fn, lMat);
      
      fn = fs["pixel_set_2"];
      CV_Assert(!fn.empty() && "\nTriangulator::Triangulate()- could not read field pixel_set_2.\n");
      cv::read(fn, rMat);
      fs.release();
      
      // Check that both matrices have identical size, >=2 columns and one channel
      CV_Assert(lMat.size()==rMat.size() && "\nTriangulator::Triangulate()- inconsistent point set sizes.\n");
      CV_Assert((lMat.cols==2 && rMat.cols==lMat.cols) && "\nTriangulator::Triangulate()- inconsistent number of columns in point set data.\n");
      CV_Assert((lMat.channels()==1 && rMat.channels()==1) && "\nTriangulator::Triangulate()- inconsistent number of channels in point set data.\n");
      const int nPoints = lMat.rows;

      // Matrix of the number of points
      cv::Mat_<double> pMat(nPoints, 3);
      cv::Point2d lPoint, rPoint;
      cv::Point3d X;
      for(int p=0; p<nPoints; ++p)
	{
	  // Read this point from left and right cameras
	  lPoint.x = lMat.at<double>(p,0); lPoint.y = lMat.at<double>(p,1);
	  rPoint.x = rMat.at<double>(p,0); rPoint.y = rMat.at<double>(p,1);
	  
	  // Triangulate this point
	  Triangulate<tm>(lPoint, rPoint, X);
	  pMat(p,0) = X.x; pMat(p,1) = X.y; pMat(p,2) = X.z;
	}

      // Open file for writing the point set
      cv::FileStorage pfile(pointsfile, cv::FileStorage::WRITE);
      CV_Assert(pfile.isOpened() && "\nTriangulator::Triangulatr()- Could not open file for writing.");
      cvWriteComment(*pfile, (char*)"Triangulated point set", 0);
      pfile << "point_set" << pMat;
      pfile.release();
      return;
    }
 
}

#endif
