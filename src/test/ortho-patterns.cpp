#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <cmath>
#include <iostream>

int main()
{
  const int N = 1024;//1080;
  const int M = 2048;//1920;
  cv::Mat img(N,M, CV_8U, cv::Scalar(255));
  CvMat cvimg = img;

  // Length of the diagonal - one of the sides
  int diag = N;

  // Inclination 1
  char filename[100];  
  for(int level=0; level<10; ++level)
    {
      // Color the black pixels
      std::cout<<"\nLevel "<<level; std::fflush( stdout );
      const int delta = diag/std::pow(2,level);
      for(int i=0; i<N; ++i)
	for(int j=0; j<M; ++j)
	  { 
	    const int indx = static_cast<int>(i/delta);
	    if(indx%2==0)
	      img.at<uchar>(i,j) = 0;
	    else
	      img.at<uchar>(i,j) = 255;
	  }

      // Save file and its inverse
      sprintf(filename, "p0-r%d.jpg", level);
      cvimg = img;
      cvSaveImage(filename, &cvimg);
      img = cv::Scalar(255)-img;
      sprintf(filename, "p0-%d.jpg", level);
      cvimg = img;
      cvSaveImage(filename, &cvimg);
    }

  // Inclination 2
  diag = M;
  for(int level=0; level<10; ++level)
    {
      std::cout<<"\nLevel "<<level; std::fflush( stdout );
      // Color the black pixels
      const int delta = diag/std::pow(2,level);
      for(int i=0; i<N; ++i)
	for(int j=0; j<M; ++j)
	  { 
	    const int indx = static_cast<int>(j/delta);
	    if(indx%2==0)
	      img.at<uchar>(i,j) = 0;
	    else
	      img.at<uchar>(i,j) = 255;
	  }
      
      // Save file and its inverse
      sprintf(filename, "p90-r%d.jpg", level);
      cvimg = img;
      cvSaveImage(filename, &cvimg);
      img = cv::Scalar(255)-img;
      sprintf(filename, "p90-%d.jpg", level);
      cvimg = img;
      cvSaveImage(filename, &cvimg);
    }
}
      
