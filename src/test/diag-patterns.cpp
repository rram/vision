#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <cmath>
#include <iostream>

int main()
{
  const int N = 1080;
  const int M = 1920;
  cv::Mat img(N,M, CV_8U, cv::Scalar(255));
  CvMat cvimg = img;

  // Length of the diagonal
  const double diag = sqrt(N*N+M*M);
  // Rotation by -theta
  const double ctheta = double(N)/diag;
  const double stheta = -double(M)/diag;
  const double R[2][2] = {{ctheta, -stheta},{stheta,ctheta}};

  // Inclination 1
  char filename[100];  
  for(int level=0; level<10; ++level)
    {
      std::cout<<"\nLevel "<<level; std::fflush( stdout );
      // Color the black pixels
      const double delta = diag/std::pow(2,level);
      for(int i=0; i<N; ++i)
	for(int j=0; j<M; ++j)
	  { 
	    // Rotated pixel
	    const double ri = R[0][0]*double(i) + R[0][1]*double(j);
	    assert(ri>=0.);
	    const int indx = static_cast<int>(ri/delta);
	    if(indx%2==0)
	      img.at<uchar>(i,j) = 0;
	    else
	      img.at<uchar>(i,j) = 255;
	  }

      // Save file and its inverse
      sprintf(filename, "p45-r%d.jpg", level);
      cvimg = img;
      cvSaveImage(filename, &cvimg);
      img = cv::Scalar(255)-img;
      sprintf(filename, "p45-%d.jpg", level);
      cvimg = img;
      cvSaveImage(filename, &cvimg);
    }

  // Inclination 2
  for(int level=0; level<10; ++level)
    {
      std::cout<<"\nLevel "<<level; std::fflush( stdout );
      // Color the black pixels
      const double delta = diag/std::pow(2,level);
      for(int i=0; i<N; ++i)
	for(int j=0; j<M; ++j)
	  { 
	    // Rotated pixel
	    const double rj = R[1][0]*double(i) + R[1][1]*double(j);
	    if(rj>=0)
	      {
		const int indx = static_cast<int>(rj/delta);
		if(indx%2==0)
		  img.at<uchar>(i,j) = 0;
		else
		  img.at<uchar>(i,j) = 255;
	      }
	    else
	      {
		const int indx = static_cast<int>(rj/delta);
		if(indx%2==0)
		  img.at<uchar>(i,j) = 255;
		else
		  img.at<uchar>(i,j) = 0;	
	      }
	  }

      // Save file and its inverse
      sprintf(filename, "p135-r%d.jpg", level);
      cvimg = img;
      cvSaveImage(filename, &cvimg);
      img = cv::Scalar(255)-img;
      sprintf(filename, "p135-%d.jpg", level);
      cvimg = img;
      cvSaveImage(filename, &cvimg);
    }
}
      
