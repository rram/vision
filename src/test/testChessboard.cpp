// Sriramajayam

#include "Chessboard.h"
#include <iostream>
#include <algorithm>

void Test(const vc::Chessboard& cb);

  
int main()
{
  // Construction from file
  vc::Chessboard cb;
  cb.Create("calib_images/leftchessboard.xml");
  Test(cb);

  // Copy constructor
  vc::Chessboard copy(cb);
  Test(copy);

  // Save contents to file and read again.
  cb.Save("chessboard2.xml");
  vc::Chessboard copy2("chessboard2.xml");
  Test(copy2);

}

void Test(const vc::Chessboard& cb)
{
  const auto& im = cb._images;
  CV_Assert(im._nImages==10);
  CV_Assert(im._imageSize==cv::Size(640,480));
  CV_Assert(im._fileNames.size()==im._nImages);
  for(unsigned int f=1; f<=im._nImages; ++f)
    {
      char filename[100];
      sprintf(filename, "calib_images/left%02d.jpg", f);
      //std::cout<<"\n"<<filename<<" should be "<<im._filenames
      CV_Assert(std::strcmp(filename, im._fileNames[f-1].c_str())==0);
    }
  CV_Assert(cb._boardSize==cv::Size(9,6));
  CV_Assert(std::abs(cb._squareSize-50.)<1.e-8);
}
