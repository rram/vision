// Sriramajayam

#include "ClickCorresponder.h"

int main()
{
  vc::ClickCorresponder clicker
    ("calib_images/left01.jpg", "calib_images/left01.jpg",  // Scene 1, left and right images
     "calib_images/left01.jpg", "calib_images/left01.jpg"); // Scene 2, left and right images

  clicker.View("1.jpg", "2.jpg", "3.jpg", "4.jpg");
  clicker.Save("pixels1.pts", "pixels2.pts");
}
