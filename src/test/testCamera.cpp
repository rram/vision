// Sriramajayam

#include "Camera.h"

// Check that a pair of cameras are identical
void CompareCameras(const vc::Camera& A, const vc::Camera& B);


int main()
{
  // Create chessboard
  vc::Chessboard cb("calib_images/leftchessboard.xml");

  // Create camera and corners for chessboard
  vc::Camera cam;
  vc::ChessboardCorners corners;
  CV_Assert(!cam.IsCalibrated());
  cam.Calibrate(cb, corners);
  CV_Assert(cam.IsCalibrated());

  // Copy constructor
  vc::Camera copy(cam);
  CompareCameras(cam, copy);
  
  // Creation from file
  copy.Save("cam.xml");
  vc::Camera copy2("cam.xml");
  CompareCameras(copy, copy2);

  // Create by setting intrinsics
  vc::Camera copy3;
  copy3.SetIntrinsics(cam.GetCameraMatrix(), cam.GetDistortionCoeffs(), cam.GetImageSize());
  CompareCameras(copy2, copy3);

  // Delayed construction
  vc::Camera copy4;
  copy4.Create("cam.xml");
  CompareCameras(copy3, copy4);
}


// Check that a pair of cameras are identical
void CompareCameras(const vc::Camera& A, const vc::Camera& B)
{
  //const double EPS = 1.e-6;

  CV_Assert(A.IsCalibrated()==B.IsCalibrated());
  CV_Assert(A.IsCalibrated()); // Comparison is not meaningful otherwise
  double diff = cv::norm(A.GetCameraMatrix(), B.GetCameraMatrix());
  CV_Assert(diff<1.e-8);
  diff = cv::norm(A.GetDistortionCoeffs(), B.GetDistortionCoeffs());
  CV_Assert(diff<1.e-8);
  CV_Assert(A.GetImageSize()==B.GetImageSize());
  cv::Mat K, DC;
  A.GetIntrinsics(K, DC);
  diff = cv::norm(K, B.GetCameraMatrix());
  CV_Assert(diff<1.e-8);
  diff = cv::norm(DC, B.GetDistortionCoeffs());
  CV_Assert(diff<1.e-8);
  cv::Mat imgA, imgB;
  for(int f=1; f<=10; ++f)
    {
      char filename[100];
      sprintf(filename, "calib_images/left%02d.jpg", f);
      A.Undistort(filename, imgA);
      B.Undistort(filename, imgB);
      diff = cv::norm(imgA, imgB);
      CV_Assert(diff<1.e-8);
    }
}
