// Sriramajayam

#include "StereoRig.h"
#include <opencv2/highgui.hpp>
#include <iostream>

// Check that a pair of StereoRigs
void CompareRigs(const vc::StereoRig& A, const vc::StereoRig& B);


int main()
{
  // Create chessboard, corners, and cameras
  vc::Chessboard board("calib_images/leftchessboard.xml");
  vc::ChessboardCorners lcorners, rcorners;
  vc::Camera lcam, rcam;
  lcam.Calibrate(board, lcorners);
  rcam.Calibrate(board, rcorners);
  CV_Assert(lcam.IsCalibrated() && rcam.IsCalibrated());
  
  // Create stereo-rig
  vc::StereoRig rig(lcam, lcorners, rcam, rcorners);
   
  // Compute the reprojection error in left and right cameras after stereo adjustment
  std::cout<<"\nLCam calib error: "<<rig.GetLeftCamera().ComputeCalibrationError(lcorners)
	   <<"\nRCam calib error: "<<rig.GetRightCamera().ComputeCalibrationError(rcorners);
  
  // Run consistency tests
  rig.ConsistencyTest(1.e-6);
  
  // Copy constructor
  vc::StereoRig copy(rig);
  CompareRigs(rig, copy);
  
  // Construct from file
  rig.Save("rig.xml");
  vc::StereoRig copy2("rig.xml");
  CompareRigs(copy, copy2);
  
  // Delayed construction
  vc::StereoRig copy3;
  copy3.Create("rig.xml");
  CompareRigs(copy2, copy3);
}


void CompareRigs(const vc::StereoRig& A, const vc::StereoRig& B)
{
  CV_Assert(A.GetImageSize()==B.GetImageSize());
  double diff = cv::norm(A.GetLeftCamera().GetCameraMatrix(), B.GetLeftCamera().GetCameraMatrix());
  CV_Assert(diff<1.e-8);
  diff = cv::norm(A.GetRightCamera().GetCameraMatrix(), B.GetRightCamera().GetCameraMatrix());
  CV_Assert(diff<1.e-8);
  diff = cv::norm(A.GetLeftCamera().GetDistortionCoeffs(), B.GetLeftCamera().GetDistortionCoeffs());
  CV_Assert(diff<1.e-8);
  diff = cv::norm(A.GetRightCamera().GetDistortionCoeffs(), B.GetRightCamera().GetDistortionCoeffs());
  CV_Assert(diff<1.e-8);
  diff = cv::norm(A.GetEssentialMatrix(), B.GetEssentialMatrix());
  CV_Assert(diff<1.e-8);
  diff = cv::norm(A.GetFundamentalMatrix(), B.GetFundamentalMatrix());
  CV_Assert(diff<1.e-8);
  cv::Mat R1, R2, T1, T2;
  A.GetCoordinateMap(R1, T1);
  B.GetCoordinateMap(R2, T2);
  diff = cv::norm(R1, R2);
  CV_Assert(diff<1.e-8);
  diff = cv::norm(T1, T2);
  CV_Assert(diff<1.e-8);
  cv::Mat A_lP, A_rP;
  A.GetProjectionMatrices(A_lP, A_rP);
  cv::Mat B_lP, B_rP;
  B.GetProjectionMatrices(B_lP, B_rP);
  diff = cv::norm(A_lP, B_lP);
  CV_Assert(diff<1.e-8);
  diff = cv::norm(A_rP, B_rP);
  CV_Assert(diff<1.e-8);
}
