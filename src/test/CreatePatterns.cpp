// Sriramajayam

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <cmath>
#include <iostream>

int main()
{
  const int N = 1080;
  const int M = 1920;
  cv::Mat img(N,M, CV_8U, cv::Scalar(255));
  CvMat cvimg = img;
  
  // White image
  //cvSaveImage("white.jpg", &cvimg);

  char filename[100];  
  for(int level=0; level<10; ++level)
    {
      // Initialize all pixels to while
      img = cv::Scalar(255);

      // Color the black pixels
      int delta = N/std::pow(2,level);
      int nblocks = N/delta;
      std::cout<<"\nDelta: "<<delta<<", nblocks: "<<nblocks;
      for(int j=0; j<M; ++j)
	for(int block=0; block<nblocks; block+=2)
	  for(int i=delta*block; i<delta*(block+1); ++i)
	    img.at<uchar>(i,j) = 0;

      // Save file and its inverse
      sprintf(filename, "h%dr.jpg", level);
      cvimg = img;
      cvSaveImage(filename, &cvimg);
      img = cv::Scalar(255)-img;
      sprintf(filename, "h%d.jpg", level);
      cvimg = img;
      cvSaveImage(filename, &cvimg);
    }

  // Vertical stripes
  for(int level=0; level<10; ++level)
    {
      // Initialize all pixels to while
      img = cv::Scalar(255);

      // Color the black pixels
      int delta = M/std::pow(2,level);
      int nblocks = M/delta;
      std::cout<<"\nDelta: "<<delta<<", nblocks: "<<nblocks;
      for(int i=0; i<N; ++i)
	for(int block=0; block<nblocks; block+=2)
	  for(int j=delta*block; j<delta*(block+1); ++j)
	    img.at<uchar>(i,j) = 0;
      
      // Save file and its inverse
      sprintf(filename, "v%dr.jpg", level);
      cvimg = img;
      cvSaveImage(filename, &cvimg);
      img = cv::Scalar(255)-img;
      sprintf(filename, "v%d.jpg", level);
      cvimg = img;
      cvSaveImage(filename, &cvimg);
    }
}
