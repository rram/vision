// Sriramajayam

#include <Triangulator.h>
#include <fstream>

int main()
{
  // Create rig
  vc::StereoRig rig("leaf_images/leaf_stereo.xml");

  // Create triangulator
  vc::Triangulator tri(rig);

  // triangulate
  tri.Triangulate<vc::TriMethod::Sym>("leaf_images/corresp.xml", "corresp.ply");
}
