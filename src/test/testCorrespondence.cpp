// Sriramajayam

#include <Corresponder.h>

constexpr int nStages = 2;
constexpr int nMaxLevels = 7;

int main()
{
  // Create corresponder
  std::vector<cv::String> scenefiles({"leaf_images/hscene.xml", "leaf_images/vscene.xml"});
  vc::Corresponder scene(cv::Size(4608, 3456), std::vector<int>({nMaxLevels,nMaxLevels}), scenefiles);
  scene.Compute<nStages, nMaxLevels>();
  scene.Save<nStages, nMaxLevels>("corresp.xml");
}
