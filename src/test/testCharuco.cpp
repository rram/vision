// Sriramajayam

#include <Charucoboard.h>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

int main()
{
  // Read file with definition of charucoboard to use
  // Create a board
  vc::Charucoboard cb("calib_images/charuco.xml");

  // Draw the board (use for printing)
  cb.Draw("cb1.jpg");
  
  // Save the details of the board 
  cb.Save("newcharuco.xml");

  // Detect markers and corners in the board
  vc::CharucoBoardImage cbImage;
  cb.Detect("cb1.jpg", cbImage);

  // Make a copy of the board with some corners occluded
  cv::Mat img = cv::imread("cb1.jpg", CV_LOAD_IMAGE_GRAYSCALE);
  cv::Size imgSize = img.size();
  cv::circle(img, cv::Point(imgSize.height/2, imgSize.width/2), imgSize.width/4, cv::Scalar(127), CV_FILLED);
  CvMat cvimg = img;
  cvSaveImage("cb2.jpg", &cvimg);
  
  // Match corners in a pair images of the board. Save corresponding points to an xml file
  cb.Match("cb1.jpg", "cb2.jpg", cv::String("MatchPts.xml"));
}
