// Sriramajayam

#include "Camera.h"
#include <iostream>

// Comare 
void Compare(const vc::ChessboardCorners& A, const vc::ChessboardCorners& B);

int main()
{
  // Construct chessboard
  vc::Chessboard cb;
  cb.Create("calib_images/leftchessboard.xml");

  // Create camera and calibrate it top populate corners
  vc::ChessboardCorners cbc;
  vc::Camera cam;
  cam.Calibrate(cb, cbc);
  CV_Assert(cam.IsCalibrated());

  // Check some attributes of corners
  const auto& nImages = cb._images._nImages;
  const unsigned int nCorners = cb._boardSize.height*cb._boardSize.width;
  CV_Assert(cbc._nImages==nImages);
  CV_Assert(cbc._cornersIn3D.size()==nImages);
  CV_Assert(cbc._cornersInImages.size()==nImages);
  CV_Assert(cbc._rvecs.size()==nImages);
  CV_Assert(cbc._tvecs.size()==nImages);
  for(unsigned int i=0; i<nImages; ++i)
    {
      CV_Assert(cbc._cornersIn3D[i].size()==nCorners);
      CV_Assert(cbc._cornersInImages[i].size()==nCorners);
      CV_Assert(cbc._rvecs[i].rows==3 && cbc._rvecs[i].cols==1);
      CV_Assert(cbc._tvecs[i].rows==3 && cbc._tvecs[i].cols==1);
    }
  // Save
  cbc.Save("chessboardcorners.xml");

  // Create from file
  vc::ChessboardCorners copy("chessboardcorners.xml");
  Compare(cbc, copy);

  // Copy constructor
  vc::ChessboardCorners copy2(copy);
  Compare(cbc, copy2);
  
}


// Comare 
void Compare(const vc::ChessboardCorners& A, const vc::ChessboardCorners& B)
{
  CV_Assert(A._nImages==B._nImages);
  CV_Assert(A._cornersIn3D.size()==B._cornersIn3D.size());
  CV_Assert(A._cornersInImages.size()==B._cornersInImages.size());
  CV_Assert(A._rvecs.size()==B._rvecs.size());
  CV_Assert(A._tvecs.size()==B._tvecs.size());
  const unsigned int nCorners = A._cornersIn3D[0].size();
  for(unsigned int i=0; i<B._nImages; ++i)
    {
      CV_Assert(A._cornersIn3D[i].size()==B._cornersIn3D[i].size());
      CV_Assert(A._cornersInImages[i].size()==B._cornersInImages[i].size());
      CV_Assert(A._rvecs[i].rows==B._rvecs[i].rows && A._rvecs[i].cols==B._rvecs[i].cols);
      CV_Assert(A._tvecs[i].rows==B._tvecs[i].rows && A._tvecs[i].cols==B._tvecs[i].cols);

      // Comapre rotations and translations
      double diff = cv::norm(A._rvecs[i], B._rvecs[i]);
      CV_Assert(diff<1.e-8);
      diff = cv::norm(A._tvecs[i], B._tvecs[i]);
      CV_Assert(diff<1.e-8);
      for(unsigned int c=0; c<nCorners; ++c)
	{
	  diff = cv::norm(A._cornersIn3D[i][c]-B._cornersIn3D[i][c]);
	  CV_Assert(diff<1.e-8);
	  diff = cv::norm(A._cornersInImages[i][c]-B._cornersInImages[i][c]);
	  CV_Assert(diff<1.e-8);
	}
    }
}
