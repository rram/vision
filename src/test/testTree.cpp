// Sriramajayam

#include "BinaryTree.h"
#include <iostream>

int main()
{
  std::vector<int> nStageLevels = {2,3};
  vc::BinaryTree tree(nStageLevels);
  const int nStages = 2;
  const int nMaxLevels = 3;
  
  vc::PixelBits<nStages,nMaxLevels> pixelbits;
  pixelbits[0][0] = false;
  pixelbits[0][1] = false;
  pixelbits[1][0] = true;
  pixelbits[1][1] = true;
  pixelbits[1][2] = true;
  CV_Assert(tree.DoesNodeExist(pixelbits)==false);
  const auto* node = tree.CreateNode(pixelbits);
  CV_Assert(node!=nullptr);
  CV_Assert(tree.DoesNodeExist(pixelbits)==true);
}
