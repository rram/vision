// Sriramajayam

#include "Camera.h"
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

namespace vc
{
  // Default constructor.
  Camera::Camera()
    :_imgSize(0,0),
     _isCalibrated(false) {}

  // Construct a calibrated camera from a file
  Camera::Camera(const cv::String xmlfile)
    :Camera()
  { Create(xmlfile); }
  
  // Copy constructor
  Camera::Camera(const Camera& obj)
    :_camMatrix(obj._camMatrix),
     _distCoeffs(obj._distCoeffs),
     _imgSize(obj._imgSize),
     _mapx(obj._mapx),
     _mapy(obj._mapy),
     _isCalibrated(obj._isCalibrated) {}
  
  
  // Destructor
  Camera::~Camera() {}

  // Returns the image size
  const cv::Size& Camera::GetImageSize() const
  { return _imgSize; }
  
  // Returns the camera matrix
  const cv::Mat& Camera::GetCameraMatrix() const
  {
    CV_Assert(_isCalibrated && "\nCamera::GetCameraMatrix()- Not calibrated.\n");
    return _camMatrix;
  }
  
  // Returns the distortion coefficients
  const cv::Mat& Camera::GetDistortionCoeffs() const
  {
    CV_Assert(_isCalibrated &&
	      "\nCamera::GetCameraMatrix()- Not calibrated.\n");
    return _distCoeffs;
  }
  
  
  // Returns the camera matrix and distortion coefficients
  void Camera::GetIntrinsics(cv::Mat& camMatrix,
			     cv::Mat& distCoeffs) const
  {
    CV_Assert(_isCalibrated &&
	      "\nCamera::GetCameraIntrinsics()- Not calibrated.\n");
    camMatrix = _camMatrix;
    distCoeffs = _distCoeffs;
    return;
  }
  
  // Set the camera matrix and distortion coefficients
  void Camera::SetIntrinsics(const cv::Mat camMatrix,
			     const cv::Mat distCoeffs,
			     const cv::Size imgSize)
  {
    _camMatrix = camMatrix;
    _distCoeffs = distCoeffs;
    _imgSize = imgSize;
    _isCalibrated = true;
    ComputeDistortionMaps();
  }
     
 
  // Main functionality: calibrate this camera
  void Camera::Calibrate(const Chessboard& cb,
			 ChessboardCorners& corners)
  {
    // Reset calibration flag
    _isCalibrated = false;

    // Copy the image size
    _imgSize = cb._images._imageSize;
    
    // Aliases
    auto& im = cb._images;
    auto& cornersIn3D = corners._cornersIn3D;
    auto& cornersInImages = corners._cornersInImages;
    auto& rvecs = corners._rvecs;
    auto& tvecs = corners._tvecs;
    const int nImages = static_cast<int>(im._nImages);
    corners._nImages = im._nImages;
    
    // Read corners in images
    cornersInImages.resize(nImages);
    const int nExpCorners = cb._boardSize.height*cb._boardSize.width;
    bool flag;
    cv::Mat img;
    const auto termcrit = cv::TermCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS,30,0.1);
    
    char filename[100];
    for(int f=0; f<nImages; ++f)
      {
	// Read this image
	std::cout<<"\nReading image "<<im._fileNames[f]; std::fflush( stdout );
	img = cv::imread(im._fileNames[f], cv::IMREAD_GRAYSCALE);
	CV_Assert(img.empty()==false && "\nCamera::Calibrate()- Could not read image.\n");
	CV_Assert(_imgSize==img.size() && "\nCamera::Calibrate()- Unexpected image size.\n");
	
	// Find corners in this image
	std::cout<<"\nFinding chessboard corners "; std::fflush( stdout );
	flag = cv::findChessboardCorners(img, cb._boardSize,
					 cornersInImages[f],
					 CV_CALIB_CB_FAST_CHECK+cv::CALIB_CB_ADAPTIVE_THRESH);
	CV_Assert(flag==true && "\nCamera::Calibrate()- Could not find corners in image.\n");
	CV_Assert( (static_cast<int>(cornersInImages[f].size())==nExpCorners) &&
		   "\nCamera::Calibrate()- Could not find all corners in image.\n");
	
	// Refine to subpixel accuracy
	// assumes at least 20px separation between corners in image
	std::cout<<"\nFinding corner subpixels "; std::fflush( stdout );
	cv::cornerSubPix(img, cornersInImages[f],
			 cv::Size(10,10), cv::Size(-1,-1), termcrit);
	
	// Plot the identified corners
	std::cout<<"\nPlotting identified corners \n"; std::fflush( stdout );
	cv::Mat img_color;
	cv::cvtColor(img, img_color, cv::COLOR_GRAY2BGR);
	cv::drawChessboardCorners(img_color, cb._boardSize,
				  cornersInImages[f], flag);
	CV_Assert(flag==true && "\nCamera::Calibrate()- pattern not found.\n");
	sprintf(filename, "calibcorners%02d.jpg", f);
	CvMat cvimg = img_color;
	cvSaveImage(filename, &cvimg);
      }
    
    // Coordinates of corners in chessboard
    cornersIn3D.resize(1);
    for(int i=0; i<cb._boardSize.height; ++i)
      for(int j=0; j<cb._boardSize.width; ++j)
	cornersIn3D[0].push_back
	  (cv::Point3f(static_cast<float>(j*cb._squareSize),
		       static_cast<float>(i*cb._squareSize), 0.));
    cornersIn3D.resize(im._nImages, cornersIn3D[0]);
    
    // Calibrate the camera
    _camMatrix = cv::Mat::eye(3,3,CV_64F);
    _distCoeffs = cv::Mat::zeros(5,1,CV_64F);
    
    // solve for parameters in an incremental way
    double rmsErr =
      cv::calibrateCamera(cornersIn3D, cornersInImages,
			  _imgSize, _camMatrix, _distCoeffs,
			  rvecs, tvecs,
			  cv::CALIB_FIX_ASPECT_RATIO+cv::CALIB_ZERO_TANGENT_DIST+cv::CALIB_FIX_K3);
    
    // Sanity checks
    CV_Assert(static_cast<int>(rvecs.size())==nImages && "\nCamera::Calibrate()- Unexpected number of rotations.\n");
    CV_Assert(static_cast<int>(tvecs.size())==nImages && "\nCamera::Calibrate()- Unexpected number of translations.\n");
    CV_Assert(rvecs[0].type()==CV_64F && "\nCamera::Calibrate()- Unexpected rotation type.\n");
    CV_Assert(tvecs[0].type()==CV_64F && "\nCamera::Calibrate()- Unexpected rotation type.\n");
    std::cout<<"\nCamera calibration, error at stage 1: "<<rmsErr;
    
    rmsErr = cv::calibrateCamera(cornersIn3D, cornersInImages,
				 _imgSize, _camMatrix, _distCoeffs,
				 rvecs, tvecs,
				 cv::CALIB_USE_INTRINSIC_GUESS+cv::CALIB_ZERO_TANGENT_DIST+cv::CALIB_FIX_K3);
    std::cout<<"\nCamera calibration, error at stage 2: "<<rmsErr;
    
    rmsErr = cv::calibrateCamera(cornersIn3D, cornersInImages,
				 _imgSize, _camMatrix, _distCoeffs,
				 rvecs, tvecs,
				 cv::CALIB_USE_INTRINSIC_GUESS+cv::CALIB_FIX_K3);
    std::cout<<"\nCamera calibration, error at stage 3: "<<rmsErr;
    
    // Calibration is done
    _isCalibrated = true;
    
    // Compute distortion maps
    ComputeDistortionMaps();
  }
  
  
  // Computes the error in calibration
  double Camera::
  ComputeCalibrationError(const ChessboardCorners& corners) const
  {
    CV_Assert(_isCalibrated && "\nCamera::ComputeCalibrationError()- Not calibrated.\n");
    
    // Reprojected points in each image
    std::vector<cv::Point2f> reproj;
    double totErr = 0.;
    int nPoints = 0;
    const auto& nImages = corners._nImages;
    const unsigned int nCorners = corners._cornersInImages[0].size();
    for(unsigned int i=0; i<nImages; ++i)
      {
	// Project points from 3D coordinates onto image plane
	cv::projectPoints(cv::Mat(corners._cornersIn3D[i]), corners._rvecs[i], corners._tvecs[i], _camMatrix, _distCoeffs, reproj);
	CV_Assert(reproj.size()==nCorners && "\nCamera::ComputeCalibrationError()- Unexpected number of projected points.\n");
	nPoints += nCorners;
	const auto& imPts = corners._cornersInImages[i];
	for(unsigned int c=0; c<nCorners; ++c)
	  totErr +=
	    (imPts[c].x-reproj[c].x)*(imPts[c].x-reproj[c].x) +
	    (imPts[c].y-reproj[c].y)*(imPts[c].y-reproj[c].y);
      }

    // Return rms error
    return std::sqrt(totErr/static_cast<double>(nPoints));
  }
  
  
  // Undistorts given image using calibrated parameters
  void Camera::Undistort(const cv::Mat& img_in,  cv::Mat& img_out) const
  {
    CV_Assert(_isCalibrated && "\nCamera::Undistort()- Camera not calibrated.\n");
    CV_Assert(!img_in.empty() && "\nCamera::Undistort()- Could not open file.\n");
    cv::remap(img_in, img_out, _mapx, _mapy, cv::INTER_LINEAR);
    return;
  }

  // Undistorts given image using calibrated parameters
  void Camera::Undistort(const cv::String imgfile, cv::Mat& img_out) const
  {
    cv::Mat img_in = cv::imread(imgfile, cv::IMREAD_GRAYSCALE);
    CV_Assert(img_in.empty()==false && "\nCamera::Undistort()- Could not open image.\n");
    Undistort(img_in, img_out);
  }

  

  // Print camera details to file
  void Camera::Save(const cv::String filename) const
  {
    std::cout<<"\nSaving camera to file "<<filename; std::fflush( stdout );
    
    CV_Assert(_isCalibrated && "\nCamera::Save()- camera not calibrated.\n");
    cv::FileStorage fs(filename, cv::FileStorage::WRITE);
    CV_Assert(fs.isOpened() && "\nCamera::Save()- could not open file.\n");

    // image size
    fs << "image_height" << _imgSize.height;
    fs << "image_width" << _imgSize.width;
    
    // camera matrix
    fs<<"cam_matrix"<<_camMatrix;
    
    // distortion coefficients
    fs<<"dist_coeffs"<<_distCoeffs;
    
    fs.release();
  }
  
  // Read camera details from file
  void Camera::Create(const cv::String filename)
  {
    std::cout<<"\nCreating camera from file "<<filename; std::fflush( stdout );
    
    cv::FileStorage fs(filename, cv::FileStorage::READ);
    CV_Assert(fs.isOpened() && "\nCamera::Create()- could not open file.\n");
    
    // Image size
    auto fn = fs["image_width"];
    CV_Assert(!fn.empty() && "\nCamera::Create()- could not read field image_width");
    cv::read(fn, _imgSize.width, 0);
    
    fn = fs["image_height"];
    CV_Assert(!fn.empty() && "\nCamera::Create()- could not read field image_height");
    cv::read(fn, _imgSize.height, 0);
    
    // Camera matrix
    fn = fs["cam_matrix"];
    CV_Assert(!fn.empty() && "\nCamera::Create()- could not read field cam_Matrix");
    cv::read(fn, _camMatrix);

    // Distortion coeffs
    fn = fs["dist_coeffs"];
    CV_Assert(!fn.empty() && "\nCamera::Create()- could not read field dist_Coeffs");
    cv::read(fn, _distCoeffs);

    fs.release();

    // Camera is assumed to be calibrated
    _isCalibrated = true;
    
    // Check camera matrix
    CV_Assert(_camMatrix.size()==cv::Size(3,3) &&
	      "\nCamera::Create()- Unexpected camera matrix dimensions.\n");
    double isSmall = std::abs(_camMatrix.at<double>(0,1))+
      std::abs(_camMatrix.at<double>(1,0))+
      std::abs(_camMatrix.at<double>(2,0))+
      std::abs(_camMatrix.at<double>(2,1));
    CV_Assert((isSmall< 1.e-6) && "\nCamera::Create()- Unexpected terms in camera matrix.\n");
    
    // Compute distortion maps
    ComputeDistortionMaps();
  }

  
  // Update the distortion mapping
  void Camera::ComputeDistortionMaps()
  {
    // no rectification done here
    cv::initUndistortRectifyMap(_camMatrix, _distCoeffs,
				cv::Mat::eye(3,3,CV_64F),
				_camMatrix, _imgSize,
				CV_32FC1, _mapx, _mapy);
  }
  
}
