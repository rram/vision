// Sriramajayam

#ifndef VC_CLICK_CORRESPONDER_H
#define VC_CLICK_CORRESPONDER_H

#include <opencv2/core.hpp>
#include <iostream>
#include <fstream>

namespace vc
{   
  class ClickCorresponder
  {
  public:

    // Constructor
    // Prompts creation of correspondence
    ClickCorresponder(const cv::String limage1, const cv::String rimage1,
		      const cv::String limage2, const cv::String rimage2);

    // Constructor- delegates to the one above
    ClickCorresponder(const std::vector<cv::String> images)
      :ClickCorresponder(images[0], images[1], images[2], images[3])
      { CV_Assert(images.size()==4 && "\nClickCorresponder: Unexpected number of images provided.\n"); }
    
    // Destructor does nothing
    inline virtual ~ClickCorresponder() {}
    
    // Disable copy
    ClickCorresponder(const ClickCorresponder&) = delete;
    
    // Displays calculated correspondence
    void View(const cv::String limage1, const cv::String rimage1,
	      const cv::String limage2, const cv::String rimage2) const;
    
    // Saves the calculated correspondence to a file
    void Save(const cv::String file1, const cv::String file2) const;

  private:
    // Mouse callback
    static void onMouse(int event, int x, int y,
			int flag, void* data);

    // Record for a single pixel
    struct Pixel
    {
      inline Pixel()
	:nClicks(0), xval(0), yval(0), flag(false) {}
      int nClicks;
      double xval, yval;
      bool flag;
    };


    // Record for collection of pixels
    struct Pixels
    {
    Pixels()
    :pixelnum(0), img(nullptr)
      { pts.clear(); }
    
      int pixelnum; // Running record of which pixel is being recorded
      std::vector<Pixel> pts;
      cv::String winName; // Window owining these pixels
      cv::Mat* img; // Image to which these pixels belong
      
      // Post-process the last pixel
      void PostProcess();
    };

    
    // Image files
    cv::String L1image, R1image, L2image, R2image;
    
    // Record of each of the four images
    Pixels L1, L2, R1, R2;
  };
}
#endif
