// Sriramajayam

#include <Utils.h>
#include <fstream>
#include <iostream>

namespace vc
{
  // Function that takes the image with a projected pattern
  // and the image with the shifted pattern.
  // A new image with a refined pattern is computed
  void ConvertShiftedImage(const std::string imfile,
			   const std::string rev_imfile,
			   const std::string simfile,
			   const std::string rev_simfile,
			   const std::string newfile,
			   const std::string rev_newfile)
  {
    // Read the image file and its reverse
    cv::Mat imgpos = cv::imread(imfile, cv::IMREAD_GRAYSCALE);
    CV_Assert(!imgpos.empty() &&
	      "\nUtils::ConvertShiftedImage()- Could not read image.\n");
    cv::Mat imgneg = cv::imread(rev_imfile, cv::IMREAD_GRAYSCALE);
    CV_Assert(!imgneg.empty() &&
	      "\nUtils::ConvertShiftedImage()- Could not read image.\n");
    
    // Read the shifted image file and its reverse
    cv::Mat s_imgpos = cv::imread(simfile, cv::IMREAD_GRAYSCALE);
    CV_Assert(!s_imgpos.empty() &&
	      "\nUtils::ConvertShiftedImage()- Could not read image.\n");
    cv::Mat s_imgneg = cv::imread(rev_simfile, cv::IMREAD_GRAYSCALE);
    CV_Assert(!s_imgneg.empty() &&
	      "\nUtils::ConvertShiftedImage()- Could not read image.\n");

    // New images to be created
    cv::Mat newimgpos(imgpos.size(), CV_8U);
    cv::Mat newimgneg(imgpos.size(), CV_8U);
    
    // Subtract the two images and identify the edge pixels
    const int Nx = imgpos.size().height;
    const int Ny = imgpos.size().width;
    std::vector<std::vector<bool>>
      GoodPixels(Nx, std::vector<bool>(Ny, true));
    
    const unsigned int Idelta = 20;
    unsigned int pval, nval, spval, snval;
    unsigned int val, sval;
    const auto white = static_cast<uchar>(255);
    const auto black = static_cast<uchar>(0);
    const auto edge = static_cast<uchar>(127);
    
    for(int i=0; i<Nx; ++i)
      for(int j=0; j<Ny; ++j)
	{
	  pval = static_cast<unsigned int>(imgpos.at<uchar>(i,j));
	  nval = static_cast<unsigned int>(imgneg.at<uchar>(i,j));
	  spval = static_cast<unsigned int>(s_imgpos.at<uchar>(i,j));
	  snval = static_cast<unsigned int>(s_imgneg.at<uchar>(i,j));

	  // Is this a good pixel
	  if(std::abs(pval-nval)<Idelta || std::abs(spval-snval)<Idelta)
	    {
	      newimgpos.at<uchar>(i,j) = edge;
	      newimgneg.at<uchar>(i,j) = edge;
	    }
	  else
	    {
	      // Bit value for image and shifted image
	      if(pval>=nval+Idelta) val = 1;
	      else val = 0;
	      if(spval>=snval+Idelta) sval = 1;
	      else sval = 0;

	      // Compare the two bits
	      if(val==sval)
		{
		  newimgpos.at<uchar>(i,j) = white;
		  newimgneg.at<uchar>(i,j) = black;
		}
	      else
		{
		  newimgpos.at<uchar>(i,j) = black;
		  newimgneg.at<uchar>(i,j) = white;
		}
	    }
	}

    // Save the files
    CvMat cvimg = newimgpos;
    cvSaveImage(newfile.c_str(), &cvimg);
    cvimg = newimgneg;
    cvSaveImage(rev_newfile.c_str(), &cvimg);
    return;
  }


  // Function that prints a point set from an xml file to a .ply
  void ConvertPointSetXml2Ply(const cv::String field,
			      const cv::String xmlfile,
			      const cv::String plyfile)
  {
    // Open the given file
    cv::FileStorage xfile(xmlfile, cv::FileStorage::READ);
    CV_Assert(xfile.isOpened() && "\nUtils::ConvertPointSetXml2Ply()- Could not open file to read.\n");
    auto fn = xfile[field];
    CV_Assert(!fn.empty() && "\nUtils::ConvertPointSetXml2Ply()- Could not open field to read.\n");
    cv::Mat_<double> data;
    cv::read(fn, data);
    xfile.release();
    
    // Check sizes
    CV_Assert(data.cols==3 && "\nUtils::ConvertPointSetXml2Ply()- Unexpected number of columns in point set.\n");

    // Open file to write
    std::fstream pfile;
    pfile.open(plyfile.c_str(), std::ios::out);
    CV_Assert(pfile.good() && "\nUtils::ConvertPointSetXml2Ply()- Could not open field to write.\n");

    // preambles
    pfile<<"ply"
	 <<"\nformat ascii 1.0"
	 <<"\nelement vertex "<<data.rows
	 <<"\nproperty float x"
	 <<"\nproperty float y"
	 <<"\nproperty float z"
	 <<"\nend_header";
    for(int i=0; i<data.rows; ++i)
      pfile<<"\n"<<data(i,0)<<"  "<<data(i,1)<<"  "<<data(i,2);
    pfile.flush();
    pfile.close();
    std::cout<<"\nSaved data for field "<<field<<" from "<<xmlfile<<" to "<<plyfile;
    std::fflush( stdout );
  }
    
  // Append multiple point set fields from xml file to a .ply
  void ConvertPointSetXmls2Ply(const std::vector<cv::String> fields, 
			       const std::vector<cv::String> xmlfiles,
			       const cv::String plyfile)
  {
    const int nFiles = static_cast<int>(xmlfiles.size());
    CV_Assert(nFiles==static_cast<int>(fields.size()) && "Utils::ConvertPointSetXmls2Ply()- Unexpected number of fields and xmlfiles.");
    cv::Mat_<double> PMat;
    for(int i=0; i<nFiles; ++i)
      {
	// Open this xmlfile
	cv::FileStorage fs(xmlfiles[i], cv::FileStorage::READ);
	CV_Assert(fs.isOpened() && "\nUtils::ConvertPointSetXml2Ply()- Could not open file to read.\n");
	auto fn = fs[fields[i]];
	CV_Assert(!fn.empty() && "\nUtils::ConvertPointSetXml2Ply()- Could not open field to read.\n");
	cv::Mat_<double> data;
	cv::read(fn, data);
	CV_Assert(!fn.empty() && "\nUtils::ConvertPointSetXml2Ply()- Expected point set to have 3 columns.");
	fs.release();
	PMat.push_back(data);
      }
    
    // Open file to write
    std::fstream pfile;
    pfile.open(plyfile.c_str(), std::ios::out);
    CV_Assert(pfile.good() && "\nUtils::ConvertPointSetXml2Ply()- Could not open field to write.\n");
    
    // preambles
    pfile<<"ply"
	 <<"\nformat ascii 1.0"
	 <<"\nelement vertex "<<PMat.rows
	 <<"\nproperty float x"
	 <<"\nproperty float y"
	 <<"\nproperty float z"
	 <<"\nend_header";
    for(int i=0; i<PMat.rows; ++i)
      pfile<<"\n"<<PMat(i,0)<<" "<<PMat(i,1)<<" "<<PMat(i,2);
    pfile.flush();
    pfile.close();
    std::cout<<"\nMerged xml files into "<<plyfile; std::fflush( stdout );    
  }
  

  // Transform points in a given file with the given field
  void ApplyRigidBodyTransformation(const cv::String rtfile, const cv::String inxmlfile, 
				    cv::String outxmlfile, const cv::String field)
  {
    // Read rotations and translations
    cv::Mat_<double> RMat, TVec;
    {
      cv::FileStorage fs(rtfile, cv::FileStorage::READ);
      CV_Assert(fs.isOpened() && "Utils::Apply rigid body transformation()- Could not open rotations/translations file.");
      auto fn = fs["rot_mat"];
      CV_Assert(!fn.empty() && "Utils::Apply rigid body transformation()- Could not find field rot_mat");
      cv::read(fn, RMat);
      CV_Assert((RMat.cols==3 && RMat.rows==3) && "Utils::Apply rigid body transformation()- Expected 3x3 matrix for rotation");
      
      fn = fs["trans_mat"];
      CV_Assert(!fn.empty() && "Utils::Apply rigid body transformation()- Could not find field trans_mat");
      cv::read(fn, TVec);
      CV_Assert((TVec.cols==1 && TVec.rows==3) && "Utils::Apply rigid body transformation()- Expected 3x1 matrix for translations");
      fs.release();
    }
    
    // Read the input pointset
    cv::Mat_<double> inPts;
    {    
      cv::FileStorage in(inxmlfile, cv::FileStorage::READ);
      CV_Assert(in.isOpened() && "Utils::Apply rigid body transformation()- Could not open input file with point set.");
      auto fn = in[field.c_str()];
      CV_Assert(!fn.empty() && "Utils::Apply rigid body transformation()- Could not find field for point set");
      cv::read(fn, inPts);
      CV_Assert((inPts.cols==3) && "Utils::Apply rigid body transformation()- Expected point set with 3 columns");
      in.release();
    }
    
    // Apply transformation
    cv::Mat_<double> outPts(inPts.rows, 3);
    for(int p=0; p<inPts.rows; ++p)
      for(int i=0; i<3; ++i)
	{
	  outPts(p,i) = 0.;
	  for(int j=0; j<3; ++j)
	    outPts(p,i) += RMat(j,i)*(inPts(p,j)-TVec(j,0));
	}
    
    // Write computed point set
    {
      cv::FileStorage out(outxmlfile, cv::FileStorage::WRITE);
      CV_Assert(out.isOpened() && "Utils::Apply rigid body transformation()- Could not open file to write computed point set.");
      out << field << outPts;
      out.release();
    }
    std::cout<<"\nTransformed points from "<<inxmlfile<<" to "<<outxmlfile; std::fflush( stdout );
    return;
  }

  
}
