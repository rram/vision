// Sriramajayam

#include "ExptDataStruct.h"
#include <map>
#include <iostream>
#include <fstream>

namespace vc
{
  // Populate public members
  ExptDataStruct::ExptDataStruct(const cv::String xmlfile)
  {
    // read the xml file
    readXML(xmlfile);
  }

  // Create folder structures and copy files for very ordered experimental data
  void ExptDataStruct::Organize(const cv::String recfile)
  {
    // Populate paths in file structure
    populatePaths();
    
    // Create folder structure
    createFolders();
    
    // Copy files
    copyFiles();

    // Record information about organization
    Record(recfile);

    // Create xml files for poses-stages
    createXMLs();
  }

  
  // Implementation of member functions
  void ExptDataStruct::copyFiles() const
  {
    copyImages(lcamImagePaths, lworkImagePaths);
    copyImages(rcamImagePaths, rworkImagePaths);
  }

  void ExptDataStruct::createFolders() const
  {
    // Create pose folders -> stage folders -> rcam, lcam sub-folders
    for(int i=0; i<nPoses; ++i)
      {
	// Create folder for this pose
	const auto& ppath = posePaths[i];
	fsys::create_directory(ppath);
	CV_Assert(fsys::is_directory(ppath));
      
	for(int stage=0; stage<nStages; ++stage)
	  {
	    // Absolute path for this stage
	    auto spath = ppath;
	    spath += stagePaths[stage];
	    fsys::create_directory(spath);
	    CV_Assert(fsys::is_directory(spath));
	  
	    // Left camera
	    fsys::path lpath = spath;
	    lpath += "/lcam/";
	    fsys::create_directory(lpath);
	    CV_Assert(fsys::is_directory(lpath));
	  
	    // Right camera
	    fsys::path rpath = spath;
	    rpath += "/rcam/";
	    fsys::create_directory(rpath);
	    CV_Assert(fsys::is_directory(rpath));
	  }
      }
    return;
  }

  void ExptDataStruct::copyImages(const std::vector<fsys::path>& camPaths,
				  const std::vector<fsys::path>& workPaths) const
  {
    // Loop over all images to be copied
    const int nImages = static_cast<int>(workPaths.size());
      
    for(int f=0; f<nImages; ++f)
      {
	const auto& cpath = camPaths[f];
	const auto& wpath = workPaths[f];
	fsys::copy(cpath, wpath);
	CV_Assert(fsys::exists(cpath) && "\nUtils::ExptDataStruct()- incorrect number of images");
	CV_Assert(fsys::is_regular_file(cpath) && "\nUtils::ExptDataStruct()- incorrect number of images");
	CV_Assert(fsys::exists(wpath));
	CV_Assert(fsys::is_regular_file(wpath));
      }
  }  



  // Assign paths for left camera images
  void ExptDataStruct::assignWorkImagePaths(const cv::String cam_name,
					    std::vector<fsys::path>& workPaths) const
  {
    CV_Assert(static_cast<int>(stagePaths.size())==nStages);
    CV_Assert(static_cast<int>(posePaths.size())==nPoses);

    // Assign paths for each image
    workPaths.clear();
    for(int pose=0; pose<nPoses; ++pose)
      {
	// Path for this pose
	const auto& ppath = posePaths[pose];

	// Trickle down the stages
	for(int stage=0; stage<nStages; ++stage)
	  {
	    // Number of levels for this stage
	    const auto& nL = nLevels[stage];
	  
	    // Absolute path for this stage
	    auto spath = ppath;
	    spath += stagePaths[stage];
	  
	    // Alternate positive and negative images
	    for(int L=0; L<nL; ++L)
	      {
		// Positive and negative images
		fsys::path pospath = spath;
		fsys::path negpath = spath;
		pospath += cam_name.c_str();
		pospath += "/";
		negpath += cam_name.c_str();
		negpath += "/";
		pospath += cv::String(std::to_string(L).c_str());
		negpath += cv::String(std::to_string(L).c_str());
		negpath += "r";
		pospath += imgExtension;
		negpath += imgExtension;
	      
		workPaths.push_back( pospath );
		workPaths.push_back( negpath );
	      }
	  }
      }
  }


  // Sorted order of camera images
  void ExptDataStruct::orderCameraImages(const fsys::path camPath,
					 std::vector<fsys::path>& orderedImagePaths) const
  {
    orderedImagePaths.clear();
  
    // Read files in the camera directory and order them by time stamp
    std::map<unsigned int, fsys::path> imgtimes;
    for(auto& img: fsys::directory_iterator(camPath))
      {
	fsys::path imgPath = static_cast<fsys::path>(img);
      
	// Check this extension. Proceed only if it an expected extension
	if(imgPath.extension()==fsys::path(imgExtension.c_str()))
	  {
	    // Check that this is a regular file
	    CV_Assert(fsys::is_regular_file(img) && "\nUtils::ExptDataStruct()- image file is not a regular file.\n");

	    // Get this filename without extension
	    cv::String filename = cv::String(imgPath.filename().stem().c_str());
	    
	    // This filename is of the form stemXXXXX, where XXXXX are numerals
	    // Extract the numeral
	    unsigned int numeral = std::stoul(filename.substr(imgStem.size()));
	    CV_Assert(imgtimes.find(numeral)==imgtimes.end() && "Utils::IdentifyDuplicates()- Fould files with identical numeral.");
	    imgtimes[numeral] = imgPath;
	  }
      }
    
    orderedImagePaths.clear();
    for(auto& img:imgtimes)
      orderedImagePaths.push_back( img.second );
    
    return;
  }


  void ExptDataStruct::populatePaths()
  {
    // Paths for camera files
    lcamPath = lcamDir.c_str();
    rcamPath = rcamDir.c_str();
    CV_Assert(fsys::exists(lcamPath) && "\nUtils::ExptDataStruct()- Left camera path does not exist");
    CV_Assert(fsys::exists(rcamPath) && "\nUtils::ExptDataStruct()- Right camera path does not exist");

    // Path for working directory
    workPath = workDir.c_str();
    CV_Assert(fsys::exists(workPath) && "\nUtils::ExptDataStruct()- working directory does not exist");

    // Check that camera and working paths exist
    CV_Assert(fsys::is_directory(lcamPath) && "\nUtils::ExptDataStruct()- Left camera path is not a directory");
    CV_Assert(fsys::is_directory(rcamPath) && "\nUtils::ExptDataStruct()- Right camera path is not a directory");;
      
    // Ordered list of paths for images in left and right cameras
    orderCameraImages(lcamPath, lcamImagePaths);
    orderCameraImages(rcamPath, rcamImagePaths);

    // Paths for poses
    posePaths.clear();
    for(int pose=0; pose<nPoses; ++pose)
      {
	fsys::path ppath = workPath;
	ppath += "/Pose";
	ppath += cv::String(std::to_string(pose)).c_str();
	ppath += "/";
	posePaths.push_back( ppath );
      }
  
    // Paths for stages relative to pose paths
    stagePaths.clear();
    for(int stage=0; stage<nStages; ++stage)
      {
	fsys::path spath = "Stage";
	spath += cv::String(std::to_string(stage)).c_str();
	spath += "/";
	stagePaths.push_back( spath );
      }
  
    // Assign paths in working directory for images from l/r cameras
    assignWorkImagePaths("lcam", lworkImagePaths);
    assignWorkImagePaths("rcam", rworkImagePaths);

    // Check that the number of available images is large enough
    const int nLimages = static_cast<int>(lcamImagePaths.size());
    const int nRimages = static_cast<int>(rcamImagePaths.size());
    int nReqImages = 0;
    for(int pose=0; pose<nPoses; ++pose)
      for(int stage=0; stage<nStages; ++stage)
	nReqImages += nLevels[stage]*2; // +,-.
    CV_Assert(nLimages>=nReqImages && "\nUtils::ExptDataStruct()- insufficient number of images from left camera");
    CV_Assert(nRimages>=nReqImages && "\nUtils::ExptDataStruct()- insufficient number of images from right camera");
  }



  // Record information about time stamps, organization
  void ExptDataStruct::Record(const cv::String recordFile) const
  {
    // Open file to write
    std::fstream recfile;
    recfile.open(recordFile.c_str(), std::ios::out);
    CV_Assert(recfile.good() && "\nUtils::ExptDataStruct()- Could not open file to record.\n");
      
    // Record the sorted list of files in the l/r cameras
    recfile<<"\nSORTED LIST OF FILES IN LEFT CAMERA"
	   <<"\n====================================";
    for(auto& img:lcamImagePaths)
      recfile<<"\n"<<img;

    recfile<<"\n\nSORTED LIST OF FILES IN RIGHT CAMERA"
	   <<"\n====================================";
    for(auto& img:rcamImagePaths)
      recfile<<"\n"<<img;
      
    // Record reorganization of files from l/r cameras
    recfile<<"\n\nORGANIZATION OF FILES FROM LEFT CAMERA (source & destination)"
	   <<"\n=======================================================";
    for(unsigned int i=0; i<lworkImagePaths.size(); ++i)
      recfile<<"\n"<<lcamImagePaths[i]<<"\t"<<lworkImagePaths[i];

    recfile<<"\n\nORGANIZATION OF FILES FROM RIGHT CAMERA (source & destination)"
	   <<"\n=======================================================";
    for(unsigned int i=0; i<rworkImagePaths.size(); ++i)
      recfile<<"\n"<<rcamImagePaths[i]<<"\t"<<rworkImagePaths[i];

    recfile.flush();
    recfile.close();

    std::cout<<"\nRecording folder and file organization in "<<recordFile;
    std::fflush( stdout );
  }


  // Print required xml files
  void ExptDataStruct::createXMLs()
  {
    // Create the directory "inxmls"
    fsys::path inxmlpath = workPath;
    inxmlpath += "/inxmls/";
    fsys::create_directory(inxmlpath);
    CV_Assert(fsys::exists(inxmlpath));
    CV_Assert(fsys::is_directory(inxmlpath));
      
    // Scene file for each pose
    for(int pose=0; pose<nPoses; ++pose)
      for(int stage=0; stage<nStages; ++stage)
	{
	  // Path for this stage
	  auto spath = workPath;
	  spath += "/Pose";
	  spath += cv::String(std::to_string(pose)).c_str();
	  spath += "/Stage";
	  spath += cv::String(std::to_string(stage)).c_str();
	    
	  // This xml file name
	  cv::String xmlfilename =
	    inxmlpath.c_str() +
	    cv::String("/pose") + cv::String(std::to_string(pose)).c_str() +
	    cv::String("-stage") + cv::String(std::to_string(stage)).c_str() + cv::String(".xml");
	    
	  // Open file
	  cv::FileStorage fs(xmlfilename, cv::FileStorage::WRITE);
	  CV_Assert(fs.isOpened() && "Utils::ExptDataStruct()- Could not open xml file to write.");

	  // Image height and width
	  cvWriteComment(*fs, (char*)"Image width", 0);
	  fs << "image_width" << imgSize.width;
	  cvWriteComment(*fs, (char*)"Image height", 0);
	  fs << "image_height" << imgSize.height;

	  // Compile list of file names for this stage
	  std::vector<cv::String> lposimgs, lnegimgs, rposimgs, rnegimgs;
	  for(int L=0; L<nLevels[stage]; ++L)
	    {
	      // Path lcam, +ve images
	      auto fpath = spath;
	      fpath += "/lcam/";
	      fpath += cv::String(std::to_string(L)).c_str();
	      fpath += imgExtension;
	      lposimgs.push_back( fpath.c_str() );

	      // Path for lcam, -ve images
	      fpath = spath;
	      fpath += "/lcam/";
	      fpath += cv::String(std::to_string(L)).c_str();
	      fpath += "r";
	      fpath += imgExtension;
	      lnegimgs.push_back( fpath.c_str() );

	      // Path rcam, +ve images
	      fpath = spath;
	      fpath += "/rcam/";
	      fpath += cv::String(std::to_string(L)).c_str();
	      fpath += imgExtension;
	      rposimgs.push_back( fpath.c_str() );

	      // Path for rcam, -ve images
	      fpath = spath;
	      fpath += "/rcam/";
	      fpath += cv::String(std::to_string(L)).c_str();
	      fpath += "r";
	      fpath += imgExtension;
	      rnegimgs.push_back( fpath.c_str() );
	    }
 
	  // Image files from l/r
	  cvWriteComment(*fs, (char*)"Left camera images with positive illumination", 0);
	  fs << "scene_lcam_pos" << lposimgs;
	  cvWriteComment(*fs, (char*)"Left camera images with negative illumination", 0);
	  fs << "scene_lcam_neg" << lnegimgs;
	  cvWriteComment(*fs, (char*)"Right camera images with positive illumination", 0);
	  fs << "scene_rcam_pos" << rposimgs;
	  cvWriteComment(*fs, (char*)"Right camera images with negative illumination", 0);
	  fs << "scene_rcam_neg" << rnegimgs;
	  fs.release();
	}

    // Copy calibration files to inxml folder. Update calibration paths.
    
    // Left
    fsys::path in = lcamCalib.c_str();
    CV_Assert(fsys::exists(in) && "ExptDataStruct: camera calibration file does not exist.");
    CV_Assert(fsys::is_regular_file(in) && "ExptDataStruct: camera calibration file has unexpected format.");
    fsys::path out = inxmlpath;
    out += in.filename();
    fsys::copy(in, out);
    CV_Assert(fsys::exists(out));
    lcamCalib = out.c_str();
    
    // Right
    in = rcamCalib.c_str();
    CV_Assert(fsys::exists(in) && "ExptDataStruct: camera calibration file does not exist.");
    CV_Assert(fsys::is_regular_file(in) && "ExptDataStruct: camera calibration file has unexpected format.");
    out = inxmlpath;
    out += in.filename();
    fsys::copy(in, out);
    CV_Assert(fsys::exists(out));
    rcamCalib = out.c_str();
    
    // Copy corners files to inxml folder. Update corners paths
    
    // Left
    in = lcamCorners.c_str();
    CV_Assert(fsys::exists(in) && "ExptDataStruct: camera corners file does not exist.");
    CV_Assert(fsys::is_regular_file(in) && "ExptDataStruct: camera corners file has unexpected format.");
    out = inxmlpath;
    out += in.filename();
    fsys::copy(in, out);
    CV_Assert(fsys::exists(out));
    lcamCorners = out.c_str();
    
    // Right
    in = rcamCorners.c_str();
    CV_Assert(fsys::exists(in) && "ExptDataStruct: camera corners file does not exist.");
    CV_Assert(fsys::is_regular_file(in) && "ExptDataStruct: camera corners file has unexpected format.");
    out = inxmlpath;
    out += in.filename();
    fsys::copy(in, out);
    CV_Assert(fsys::exists(out));
    rcamCorners = out.c_str();
    
    // Create folder for writing xmls
    fsys::path outxml = workPath;
    outxml += "/outxmls/";
    fsys::create_directory(outxml);
    CV_Assert(fsys::exists(outxml));
    CV_Assert(fsys::is_directory(outxml));
  }
  
  // Create a template xml file
  void ExptDataStruct::createTemplate(const cv::String xmlfile) const
  {
    std::cout<<"\n"<<xmlfile<<" does not exist. Creating it as a template file. "
	     <<"\nRe-run this executable with the fields populated.\n";
    std::fflush( stdout );
      
    cv::FileStorage fs(xmlfile, cv::FileStorage::WRITE);
    CV_Assert(fs.isOpened() && "Utils::ExptDataStruct()- could not create template file.");
      
    cvWriteComment(*fs, (char*)"Absolute path to left camera files. Trailing / is not important.", 0);
    fs << "lcam_dir" <<"path_here";

    cvWriteComment(*fs, (char*)"Absolute path to right camera files. Trailing / is not important.", 0);
    fs << "rcam_dir" <<"path_here";

    cvWriteComment(*fs, (char*)"Absolute path for working directory. Should exist. Trailing / is not important.", 0);
    fs << "work_dir" <<"path_here";

    cvWriteComment(*fs, (char*)"Absolute path to left camera calibration xml file.", 0);
    fs << "lcam_calib" << "path_here";

    cvWriteComment(*fs, (char*)"Absolute path to right camera calibration xml file.", 0);
    fs << "rcam_calib" << "path_here";

    cvWriteComment(*fs, (char*)"Absolute path to left camera corners xml file.", 0);
    fs << "lcam_corners" << "path_here";

    cvWriteComment(*fs, (char*)"Absolute path to right camera corners xml file.", 0);
    fs << "rcam_corners" << "path_here";

    cvWriteComment(*fs, (char*)"Image stem, i.e., non-numeric part of file name. Eg: IMG_, CASE SENSITIVE.", 0);
    fs << "img_stem" << "eg:IMG_";
    
    cvWriteComment(*fs, (char*)"Image extension, i.e., image format. NO DOT. CASE SENSITIVE.", 0);
    fs << "img_extension" << "eg:JPG";

    cvWriteComment(*fs, (char*)"Image height", 0);
    fs << "image_height" << "value";

    cvWriteComment(*fs, (char*)"Image width", 0);
    fs << "image_width" << "value";

    cvWriteComment(*fs, (char*)"Number of poses", 0);
    fs << "num_poses" << "value";

    cvWriteComment(*fs, (char*)"Number of stages (patterns used)", 0);
    fs << "num_stages" << "value";

    cv::Mat_<int> mLevels;
    cvWriteComment(*fs, (char*)"Number of levels in corresponding stages (in order)", 0);
    fs << "num_levels" << mLevels;
      
    fs.release();
    exit(1);
  }
    
  // Organize data through constructor
  void ExptDataStruct::readXML(const cv::String xmlfile)
  {
    // Read the xml file details
    cv::FileStorage fs(xmlfile, cv::FileStorage::READ);
    if(!fs.isOpened())
      createTemplate(xmlfile);
      
    auto fn = fs["lcam_dir"];
    CV_Assert(!fn.empty() && "Utils::ExptDataStruct()- Could not find field lcam_dir");
    std::vector<cv::String> strings;
    cv::read(fn, strings);
    CV_Assert(strings.size()==1);
    lcamDir = strings[0];

    fn = fs["rcam_dir"];
    CV_Assert(!fn.empty() && "Utils::ExptDataStruct()- Could not find field rcam_dir");
    strings.clear();
    cv::read(fn, strings);
    CV_Assert(strings.size()==1);
    rcamDir = strings[0];
  
    fn = fs["work_dir"];
    CV_Assert(!fn.empty() && "Utils::ExptDataStruct()- Could not find field work_dir");
    strings.clear();
    cv::read(fn, strings);
    CV_Assert(strings.size()==1);
    workDir = strings[0];

    // Calibration files
    fn = fs["lcam_calib"];
    CV_Assert(!fn.empty() && "Utils::ExptDataStruct()- Could not find field lcam_calib");
    strings.clear();
    cv::read(fn, strings);
    CV_Assert(strings.size()==1);
    lcamCalib = strings[0];

    fn = fs["rcam_calib"];
    CV_Assert(!fn.empty() && "Utils::ExptDataStruct()- Could not find field rcam_calib");
    strings.clear();
    cv::read(fn, strings);
    CV_Assert(strings.size()==1);
    rcamCalib = strings[0];

    // Files with corners
    fn = fs["lcam_corners"];
    CV_Assert(!fn.empty() && "Utils::ExptDataStruct()- Could not find field lcam_corners");
    strings.clear();
    cv::read(fn, strings);
    CV_Assert(strings.size()==1);
    lcamCorners = strings[0];

    fn = fs["rcam_corners"];
    CV_Assert(!fn.empty() && "Utils::ExptDataStruct()- Could not find field rcam_corners");
    strings.clear();
    cv::read(fn, strings);
    CV_Assert(strings.size()==1);
    rcamCorners = strings[0];
    
    // Image extension
    fn = fs["img_extension"];
    CV_Assert(!fn.empty() && "Utils::ExptDataStruct()- Could not find field img_extension");
    strings.clear();
    cv::read(fn, strings);
    CV_Assert(strings.size()==1);
    imgExtension = "." + strings[0];

        // Image stem
    fn = fs["img_stem"];
    CV_Assert(!fn.empty() && "Utils::ExptDataStruct()- Could not find field img_stem");
    strings.clear();
    cv::read(fn, strings);
    CV_Assert(strings.size()==1);
    imgStem = strings[0];
    
    // Image sizes
    fn = fs["image_height"];
    CV_Assert(!fn.empty() && "Utils::ExptDataStruct()- Could not find field image_height");
    cv::read(fn, imgSize.height, 0);
    CV_Assert(imgSize.height>0);

    fn = fs["image_width"];
    CV_Assert(!fn.empty() && "Utils::ExptDataStruct()- Could not find field image_width");
    cv::read(fn, imgSize.width, 0);
    CV_Assert(imgSize.width>0);

    // Number of poses
    fn = fs["num_poses"];
    CV_Assert(!fn.empty() && "Utils::ExptDataStruct()- Could not find field num_poses");
    cv::read(fn, nPoses, 0);
    CV_Assert(nPoses>=1);
  
    // Number of stages
    fn = fs["num_stages"];
    CV_Assert(!fn.empty() && "Utils::ExptDataStruct()- Could not find field num_stages");
    cv::read(fn, nStages, 0);
    CV_Assert(nStages>=1);

    // Number of levels per stage
    fn = fs["num_levels"];
    CV_Assert(!fn.empty() && "Utils::ExptDataStruct()- Could not find field num_levels");
    cv::Mat_<int> mLevels;
    cv::read(fn, mLevels);
    CV_Assert(mLevels.rows==nStages && "Utils::ExptDataStruct()- Number of levels is expected for each stage");

    nLevels.resize(nStages);
    for(int i=0; i<nStages; ++i)
      {
	nLevels[i] = mLevels(i,0);
	//CV_Assert(nLevels[i]>=1 && "Utils::ExptDataStruct()- NUmber of levels for a stage should be at least 1");
      }
    fs.release();
    return;
  }

}

