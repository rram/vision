// Sriramajayam

#ifndef VISION_BTREE_NODE
#define VISION_BTREE_NODE

#include "DataTypes.h"
#include <cassert>

namespace vc
{
  struct Node
  {
    Node* LeftBranch; // Pointer to child representing left branch
    Node* RightBranch; // Pointer to child representing right branch
    Node* ParentBranch; // Pointer to parent branch
    int Level; // Level number of this node, starting from level 0
    int Stage; // Stage 0,1,2,...
    std::vector<Pixel>* data; // Data in this leaf is the collection of pixels it contains
        
    // Default constructor
    inline Node()
    {
      // Initialize all pointers to be null
      LeftBranch = nullptr;
      RightBranch = nullptr;
      ParentBranch = nullptr;
      Level = -1;
      Stage = -1;
      data = nullptr;
    }

    // Destructor. Destroy data
    inline ~Node()
    {
      if(LeftBranch!=nullptr) delete LeftBranch;
      if(RightBranch!=nullptr) delete RightBranch;
      if(data!=nullptr) delete data;
    }
  };
}


#endif
