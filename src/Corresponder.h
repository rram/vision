// Sriramajayam

#ifndef _VISION_CORRESPONDER_H
#define _VISION_CORRESPONDER_H

#include <vector>
#include <iostream>
#include <random>
#include "BinaryTree.h"
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <array>
#include <fstream>

namespace vc
{
  class Corresponder
  {
  public:

    // Constructor
    Corresponder(const cv::Size imSize,
		 const std::vector<int> nstagelevels,
		 const std::vector<cv::String> sc);
    
    // Copy constructor- disabled
    Corresponder(const Corresponder&) = delete;
    
    // Destructor: does nothing
    inline virtual ~Corresponder() {}
    
    // Main functionality- build correspondence
    template<long unsigned int nstages, long unsigned int nmaxlevels>
      void Compute();
    
    // Returns access to the binary tree for the left camera
    inline BinaryTree& GetLeftBinaryTree()
    { return _leftBtree; }
    
    // Returns access to the binary tree for the right camera
    inline BinaryTree& GetRightBinaryTree()
    { return _rightBtree; }
    
    // Returns the number of stages
    inline int GetNumStages() const
    { return nStages; }

    // Returns the number of levels for a stage
    inline int GetNumLevels(const int stage) const
    { return nStageLevels[stage]; }
    
    // Visualize correspondence
    template<long unsigned int nstages, long unsigned int nmaxlevels>
      void VisualizeCorrespondence(const char* pre) const;

    // Saves the average pixel coordinates within each corresponding block
    template<long unsigned int nstages, long unsigned int nmaxlevels>
      void Save(const cv::String) const;
    
  private:
    // Members
    const cv::Size _imgSize;
    const int nStages;
    const std::vector<int> nStageLevels;
    const int nMaxLevels;
    const std::vector<cv::String> Scenefiles;
    BinaryTree _leftBtree;
    BinaryTree _rightBtree;
    bool once_flag;

    // Helper functions

    // Assign colors to nodes
    template<long unsigned int nstages, long unsigned int nmaxlevels>
      void InspectLeafNodes(Node* node, PixelBits<nstages, nmaxlevels> pixelbits,
			    cv::Mat& limg, cv::Mat& rimg,
			    std::mt19937& gen,
			    std::uniform_int_distribution<>& dist) const;

    // Computes avergae pixel coordinates within each 
    template<long unsigned int nstages, long unsigned int nmaxlevels>
      void PixelateCorrespondingLeafNodes(Node* node, PixelBits<nstages, nmaxlevels> pixelbits,
					  cv::Mat_<double>& lMat, cv::Mat_<double>& rMat, cv::Mat_<int>& nMat) const;

    // Visualize acquired data with specified data
    template<long unsigned int nstages, long unsigned int nmaxlevels>
      static void VisualizeScene
      (const std::vector<int>& nstagelevels,
       const ImageBits<nstages, nmaxlevels>& ImgBits,
       const Boolean2D& GoodPixels,
       const Boolean2D& EdgePixels,
       const char* pre);
    
    
    // Create binary tree
    template<long unsigned int nstages, long unsigned int nmaxlevels>
      static void ReadScene(const int stage,
			    const int nLevels,
			    const cv::String scenefile,
			    const CamLabel camlabel,
			    const cv::Size &imgSize,
			    Boolean2D& GoodPixels,
			    ImageBits<nstages, nmaxlevels>& ImgBits);
    
    // Identify edge pixels in a scene. iscount confidence in edge pixels
    template<long unsigned int nstages, long unsigned int nmaxlevels>
      static void GetEdgePixels
      (const std::vector<int>& nstagelevels,
       const ImageBits<nstages, nmaxlevels>& ImBits,
       const Boolean2D& GoodPixels,
       Boolean2D& EdgePixels);
      
    // Builds a binary tree given the scene data
    template<long unsigned int nstages, long unsigned int nmaxlevels>
      static void BuildTree(const ImageBits<nstages, nmaxlevels>& ImBits,
			    const Boolean2D& GoodPixels,
			    const Boolean2D& EdgePixels,
			    BinaryTree& btree);
  };


  // Constructor
  Corresponder::Corresponder(const cv::Size imSize,
			     const std::vector<int> nstagelevels,
			     const std::vector<cv::String> sc)
    :_imgSize(imSize),
    nStages(nstagelevels.size()),
    nStageLevels(nstagelevels),
    nMaxLevels(*std::max_element(nStageLevels.begin(),
				 nStageLevels.end())),
    Scenefiles(sc),
    _leftBtree(nStageLevels),
    _rightBtree(nStageLevels),
    once_flag(false)
      {
	// Sanity checks for data provided
	CV_Assert(nStages>0);
	CV_Assert(Scenefiles.size()==static_cast<unsigned int>(nStages));
	for(auto& it:nStageLevels)
	  CV_Assert(it>0);
      }
  
  // Main functionality: build correspondence
  template<long unsigned int nstages, long unsigned int nmaxlevels>
    void Corresponder::Compute()
  {
    // Ensure that this function is called only once
    CV_Assert(once_flag==false);
    once_flag = true;

    // Sanity checks
    CV_Assert(nstages==nStages);
    CV_Assert(nmaxlevels==nMaxLevels);
    
    // Image dimensions
    const unsigned int Nx = static_cast<unsigned int>(_imgSize.height);
    const unsigned int Ny = static_cast<unsigned int>(_imgSize.width);
    
    // Create pixel bits for this image
    ImageBits<nstages, nmaxlevels>
      ImgBits(Nx, std::vector<PixelBits<nstages, nmaxlevels>>(Ny));
    
    // Track pixels with confidence in contrast
    Boolean2D GoodPixels(Nx, std::vector<bool>(Ny));
    
    // Track pixels lying on the edges of patterns
    Boolean2D EdgePixels(Nx, std::vector<bool>(Ny));
    
    // Read files at each stage for the left camera
    std::cout<<"\n\n\nBuilding scene for left camera"
	     <<"\n================================\n";
    std::fflush( stdout );
    
    // Assume that all pixels are good and no pixel is on the edge.
    for(auto& it:GoodPixels)
      std::fill(it.begin(), it.end(), true);
    
    for(int stage=0; stage<nStages; ++stage)
      {
	// Construct tree for this stage as observed by the left camera
	std::cout<<"\nStage: "<<stage
		 <<"\n============\n";
	std::fflush( stdout );
	
	ReadScene(stage, nStageLevels[stage], Scenefiles[stage], CamLabel::Left,
		  _imgSize, GoodPixels, ImgBits);
      }
    
    // Identify edge pixels based on image bits from left camera
    GetEdgePixels(nStageLevels, ImgBits, GoodPixels, EdgePixels);
    //VisualizeScene(nStageLevels, ImgBits, GoodPixels, EdgePixels, (char*)"left");
    BuildTree(ImgBits, GoodPixels, EdgePixels, _leftBtree);
    
    // Read files at each stage for the right camera
    std::cout<<"\n\n\nBuilding scene for right camera"
	     <<"\n================================\n";
    std::fflush( stdout );
    
    // Assume that all pixels are good and no pixel is on the edge.
    for(auto& it:GoodPixels)
      std::fill(it.begin(), it.end(), true);
    
    for(int stage=0; stage<nStages; ++stage)
      {
	// Construct tree for this stage as observed by the right camera
	std::cout<<"\nStage: "<<stage
		 <<"\n============\n";
	std::fflush( stdout );

	ReadScene(stage, nStageLevels[stage], Scenefiles[stage], CamLabel::Right,
		  _imgSize, GoodPixels, ImgBits);
      }
      
    // Identify edge pixels based on image bits from right camera
    GetEdgePixels(nStageLevels, ImgBits, GoodPixels, EdgePixels);
    //VisualizeScene(nStageLevels, ImgBits, GoodPixels, EdgePixels, (char*)"right");
    BuildTree(ImgBits, GoodPixels, EdgePixels, _rightBtree);
    
    // Visualize correspondence between left and right cameras.
    std::cout<<"\nVisualizing correspondence: "; std::fflush( stdout );
    VisualizeCorrespondence<nstages, nmaxlevels>((char*)"");
    return; 
  }
  

  // Reads image files into binary data
  template<long unsigned int nstages, long unsigned int nmaxlevels>
    void Corresponder::
    ReadScene(const int stage,
	      const int nLevels,
	      const cv::String scenefile,
	      const CamLabel camlabel, 
	      const cv::Size& imgSize,
	      Boolean2D& GoodPixels,
	      ImageBits<nstages, nmaxlevels>& ImgBits)
  {
    const int Nx = imgSize.height;
    const int Ny = imgSize.width;
    CV_Assert( (static_cast<int>(ImgBits.size())==Nx &&
		static_cast<int>(ImgBits[0].size())==Ny) &&
	       "\nCorresponder::ReadScene()- image bits expected to be sized at construction.\n");
    
    // Open the scene file
    std::cout<<"\nReading file "<<scenefile; std::fflush( stdout );
    cv::FileStorage fs(scenefile, cv::FileStorage::READ);
    CV_Assert(fs.isOpened() &&
	      "\nCorresponder::ReadScene()- Could not open scene file.\n");

    // +ve and -ve image files for this stage
    std::vector<cv::String> fpos, fneg;  
    if(camlabel==CamLabel::Left) // Left camera
      {
	// positive
	auto fn = fs["scene_lcam_pos"];
	CV_Assert(!fn.empty() && "\nCorresponder::ReadScene()- could not read scene_lcam_pos");
	cv::read(fn, fpos);
	// negative
	fn = fs["scene_lcam_neg"];
	CV_Assert(!fn.empty() && "\nCorresponder::ReadScene()- could not read scene_lcam_neg");
	cv::read(fn, fneg);
      }
    else // Right camera
      {
	// positive
	auto fn = fs["scene_rcam_pos"];
	CV_Assert(!fn.empty() && "\nCorresponder::ReadScene()- could not read scene_rcam_pos");
	cv::read(fn, fpos);
	// negative
	fn = fs["scene_rcam_neg"];
	CV_Assert(!fn.empty() && "\nCorresponder::ReadScene()- could not read scene_rcam_neg");
	cv::read(fn, fneg);
      }
    fs.release();
    
    // Sanity checks
    CV_Assert(fpos.size()>=static_cast<unsigned int>(nLevels)
	      && "\nCorresponder::ReadScene()- inconsistent #images in positive config.\n");
    CV_Assert(fneg.size()>=static_cast<unsigned int>(nLevels)
	      && "\nCorresponder::ReadScene()- inconsistent #images in negative config.\n");
    
    // Read image bits.
    for(int level=0; level<nLevels; ++level)
      {
	// Read positive image
	std::cout<<"\nReading file: "<<fpos[level]; std::fflush( stdout );
	cv::Mat imgpos = cv::imread(fpos[level], cv::IMREAD_GRAYSCALE);
	CV_Assert(!imgpos.empty() && "\nCorresponder::ReadScene()- Could not read image.\n");
	CV_Assert((imgpos.rows==Nx && imgpos.cols==Ny) &&
		  "\nCorresponder::ReadScene()- Unexpected image size.\n");
	
	// Read negative image
	std::cout<<"\nReading file: "<<fneg[level]; std::fflush( stdout );
	cv::Mat imgneg = cv::imread(fneg[level], cv::IMREAD_GRAYSCALE);
	CV_Assert(!imgneg.empty() && "\nCorresponder::ReadScene()- Could not read image.\n");
	CV_Assert((imgneg.rows==Nx && imgneg.cols==Ny) &&
		  "\nCorresponder::ReadScene()- Unexpected image size.\n");
	
	// Subtract the negative image from the positive and record bits
	std::cout<<"\nRecording image bits\n"; std::fflush( stdout );
	unsigned int pval, nval;
	const unsigned int Idelta = 20;
	// TODO: Intensity difference between positive and negative illumination
	
	for(int i=0; i<Nx; ++i)
	  {
	    CV_Assert(Ny==static_cast<int>(ImgBits[i].size()));
	    for(int j=0; j<Ny; ++j)
	      if(GoodPixels[i][j]) // Record data only if this is still a good pixel
		{
		  // Bits at this pixel
		  auto& pixelbits = ImgBits[i][j];
		  
		  // Intensities at this pixel from positive and negative images
		  pval = static_cast<unsigned int>(imgpos.at<uchar>(i,j));
		  nval = static_cast<unsigned int>(imgneg.at<uchar>(i,j));
		  
		  if(pval>=nval+Idelta) // illuminated
		    pixelbits[stage][level] = true;
		  else if(nval>=pval+Idelta) // not illuminated
		    pixelbits[stage][level] = false;
		  else // Unclear if this pixel is illuminated or not.
		    // Image bit in this case should not be used.
		    GoodPixels[i][j] = false;
		}
	  }
      }
    
    return;
    // Done.
  }
  
  
  // Discount confidence in edge pixels
  template<long unsigned int nstages, long unsigned int nmaxlevels>
    void Corresponder::
    GetEdgePixels(const std::vector<int>& nstagelevels,
		  const ImageBits<nstages, nmaxlevels>& ImgBits,
		  const Boolean2D& GoodPixels,
		  Boolean2D&  EdgePixels)
  {
    const unsigned int Nx = ImgBits.size();
    const unsigned int Ny = ImgBits[0].size();
    CV_Assert((ImgBits.size()==GoodPixels.size() && ImgBits[0].size()==GoodPixels[0].size()) &&
	      "\nCorresponder::GetEdgePixels()- Unexpected array sizes.\n");
    
    // Initialize all pixels assuming that they are not on the edge.
    EdgePixels.resize(Nx, std::vector<bool>(Ny));
    for(auto& it:EdgePixels)
      std::fill(it.begin(), it.end(), false);
    
    // Label pixels on the edge of the image as edges
    for(unsigned int j=0; j<Ny; ++j)
      {
	EdgePixels[0][j] = true;
	EdgePixels[Nx-1][j] = true;
      }
    for(unsigned int i=0; i<Nx; ++i)
      {
	EdgePixels[i][0] = true;
	EdgePixels[i][Ny-1] = true;
      }
    
    // Examine only pixels that are labeled as good.
    // If a pixel is on the boundary at any level, label it as an edge pixel.
    bool valPlus, valMinus;
    for(unsigned int i=1; i<Nx-1; ++i) // Note the limits to avoid edge of image
      for(unsigned int j=1; j<Ny-1; ++j)
	{
	  // Horizontals
	  if(GoodPixels[i+1][j] && GoodPixels[i-1][j])
	    for(unsigned int stage=0; stage<nstages; ++stage)
	      {
		const auto& nLevels = nstagelevels[stage];
		const auto& xp_y = ImgBits[i+1][j][stage];
		const auto& xm_y = ImgBits[i-1][j][stage];
		for(int level=0; level<nLevels; ++level)
		  {
		    valPlus = xp_y[level];
		    valMinus = xm_y[level];
		    if(valPlus!=valMinus)
		      EdgePixels[i][j] = true;
		  }
	      }
	  
	  // Verticals
	  if(GoodPixels[i][j-1] && GoodPixels[i][j+1])
	    for(unsigned int stage=0; stage<nstages; ++stage)
	      {
		const auto& nLevels = nstagelevels[stage];
		const auto& x_yp = ImgBits[i][j+1][stage];
		const auto& x_ym = ImgBits[i][j-1][stage];
		for(int level=0; level<nLevels; ++level)
		  {
		    valPlus = x_yp[level];
		    valMinus = x_ym[level];
		    if(valPlus!=valMinus)
		      EdgePixels[i][j] = true;
		  }
	      }
	}
    return;
  }
  
  // Builds a binary tree given the scene data
  template<long unsigned int nstages, long unsigned int nmaxlevels>
    void Corresponder::
    BuildTree(const ImageBits<nstages, nmaxlevels>& ImgBits,
	      const Boolean2D& GoodPixels,
	      const Boolean2D& EdgePixels,
	      BinaryTree& btree)
  {
    const unsigned int Nx = GoodPixels.size();
    const unsigned int Ny = GoodPixels[0].size();
    Pixel pixel;
    for(unsigned int i=0; i<Nx; ++i)
      {
	pixel.first = i;
	for(unsigned int j=0; j<Ny; ++j)
	  if(GoodPixels[i][j] && !EdgePixels[i][j])
	    {
	      pixel.second = j;
	      btree.Insert(pixel, ImgBits[i][j]);
	    }
      }
    return;
  }
  
  
  // Visualize acquired data with specified camera
  template<long unsigned int nstages, long unsigned int nmaxlevels>
    void Corresponder::
    VisualizeScene(const std::vector<int>& nstagelevels,
		   const ImageBits<nstages, nmaxlevels>& ImgBits,
		   const Boolean2D& GoodPixels,
		   const Boolean2D& EdgePixels,
		   const char* pre)
  {
    // Create binary images for this camera's scene for each level
    const unsigned int Nx = ImgBits.size();
    const unsigned int Ny = ImgBits[0].size();
    cv::Mat imgbin(Nx, Ny, CV_8U);
    
    bool isGood, isEdge;
    unsigned int val;
    char filename[100];
    for(unsigned int stage=0; stage<nstages; ++stage)
      {
	const auto& nLevels = nstagelevels[stage];
	for(int level=0; level<nLevels; ++level)
	  {
	    // Create a binary image for this level
	    for(unsigned int i=0; i<Nx; ++i)
	      for(unsigned int j=0; j<Ny; ++j)
		{
		  // If this is a good pixel/not an edge pixel, set value to be b/w
		  // Otherwise, color the pixel gray
		  const auto& pixelbits = ImgBits[i][j];
		  isGood = GoodPixels[i][j];
		  isEdge = EdgePixels[i][j];
		  val = 255*static_cast<unsigned int>(pixelbits[stage][level]);
		  if(isGood && !isEdge)
		    imgbin.at<uchar>(i,j) = static_cast<uchar>(val); // good pixel
		  else
		    imgbin.at<uchar>(i,j) = static_cast<uchar>(127);
		}
	    // Save this image
	    sprintf(filename, "%s-S%d-L%02d.png", pre, stage, level);
	    //cv::imwrite(str, imgbin); // Bug in library
	    std::cout<<"\nSaving file "<<filename; std::fflush( stdout );
	    CvMat cvimg = imgbin;
	    cvSaveImage(filename, &cvimg);
	  }
      }
    return;
  }
  
  
  // Visualize correspondence between left and right cameras
  template<long unsigned int nstages, long unsigned int nmaxlevels>
    void Corresponder::
    VisualizeCorrespondence(const char* pre) const
  {
    CV_Assert(nstages==nStages);
    CV_Assert(nmaxlevels==nMaxLevels);
    
    const unsigned int Nx = static_cast<unsigned int>(_imgSize.height);
    const unsigned int Ny = static_cast<unsigned int>(_imgSize.width);
    
    cv::Mat limg = cv::Mat::zeros(Nx, Ny, CV_8UC3); // Left image
    cv::Mat rimg = cv::Mat::zeros(Nx, Ny, CV_8UC3); // Right image
    
    std::array<long unsigned int, nstages> maxNumber;
    for(int stage=0; stage<nStages; ++stage)
      maxNumber[stage] = std::pow(2, nStageLevels[stage]);

    std::array<long unsigned int, nstages> number;
    for(int stage=0; stage<nStages; ++stage)
      number[stage] = 0;

    // Random color generation
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist(1, 255);

    // Root node for the left tree
    auto* lRoot = _leftBtree.GetRootNode();
    PixelBits<nstages, nmaxlevels> pixelbits;

    // Inspect all existent leaf nodes
    InspectLeafNodes(lRoot, pixelbits, limg, rimg, gen, dist);

    // Save the correspondence images
    char filename[100];
    sprintf(filename, "%sleft-cor.png", pre);
    CvMat cvimg = limg;
    cvSaveImage(filename, &cvimg); //cv::imwrite(); // Bug in library
    
    sprintf(filename, "%sright-cor.png", pre);
    CvMat cvrimg = rimg;
    cvSaveImage(filename, &cvrimg);
    return;
  }
      
  
  // Assign colors to nodes
  template<long unsigned int nstages, long unsigned int nmaxlevels>
    void Corresponder::InspectLeafNodes(Node* node, PixelBits<nstages, nmaxlevels> pixelbits,
					cv::Mat& limg, cv::Mat& rimg,
					std::mt19937& gen,
					std::uniform_int_distribution<>& dist) const
  {
    // If this is a null node, return.
    if(node==nullptr)
      return;
    
    // This is not a null node.
    const auto& stage = node->Stage;
    const auto& level = node->Level;
    
    // Record this bit information if it is not a stage-switching node
    if(level>0)
      {
	if(node->ParentBranch->LeftBranch==node)
	  pixelbits[stage][level-1] = false;
	else
	  pixelbits[stage][level-1] = true;
      }
    
    // Is it a pixel bin?
    if(stage==nStages-1 && level==nStageLevels[nStages-1])
      {
	CV_Assert(_leftBtree.Search(pixelbits)==node);

	// Inspect the corresponding leaf in the right tree using the given pixelbits.
	if(node->data!=nullptr)
	  {
	    const auto* rnode = _rightBtree.Search(pixelbits);
	    if(rnode!=nullptr)
	      if(rnode->data!=nullptr)
		{
		  // Generate a random color for this block
		  cv::Vec3b color;
		  color.val[0] = dist(gen);
		  color.val[1] = dist(gen);
		  color.val[2] = dist(gen);
		  
		  // Assign this color to pixels in left and right block
		  for(auto& it:*(node->data))
		    limg.at<cv::Vec3b>(it.first, it.second) = color;
		  for(auto& it:*(rnode->data))
		    rimg.at<cv::Vec3b>(it.first, it.second) = color;
		}
	  }
      }
    else
      {
	// Inspect its left and right children.
	InspectLeafNodes(node->LeftBranch, pixelbits, limg, rimg, gen, dist);
	InspectLeafNodes(node->RightBranch, pixelbits, limg, rimg, gen, dist);
      }
    return;
  }

  // Saves the average pixel coordinates within each corresponding block
  // The format is xpixel1, ypixel, xpixel2, ypixel2
  template<long unsigned int nstages, long unsigned int nmaxlevels>
    void Corresponder::Save(const cv::String xmlfile) const
  {
    CV_Assert(nstages==nStages);
    CV_Assert(nmaxlevels==nMaxLevels);
           
    // Root node for the left tree
    auto* lRoot = _leftBtree.GetRootNode();
    PixelBits<nstages, nmaxlevels> pixelbits;

    // Inspect all existent leaf nodes and save their average coordinate
    // Each entry in the matrix is (avg_xpixel, avg_ypixel in block)
    cv::Mat_<double> lMat, rMat;
    // Each entry in the matrix is # pixels in corresponding blocks from scene 1 and scene 2
    cv::Mat_<int> nMat;
    PixelateCorrespondingLeafNodes(lRoot, pixelbits, lMat, rMat, nMat);
    CV_Assert((lMat.size()==rMat.size() && lMat.rows==nMat.rows && nMat.cols==2) && "\nCorresponder::Save()- inconsistent correspondence.\n");

    // Save corresponding pixels to file
    std::cout<<"\nSaving averaged-corresponding pixels to "<<xmlfile;
    std::fflush( stdout );
    cv::FileStorage fs(xmlfile, cv::FileStorage::WRITE);
    cvWriteComment(*fs, (char*)"Pixel set 1", 0);
    fs << "pixel_set_1" << lMat;
    cvWriteComment(*fs, (char*)"Pixel set 2", 0);
    fs << "pixel_set_2" << rMat;
    cvWriteComment(*fs, (char*)"Number of pixels in corresponding blocks", 0);
    fs << "num_pixels" << nMat;
    fs.release();
  }

  // Computes avergae pixel coordinates within each 
  template<long unsigned int nstages, long unsigned int nmaxlevels>
    void Corresponder::
    PixelateCorrespondingLeafNodes(Node* node,
				   PixelBits<nstages, nmaxlevels> pixelbits,
				   cv::Mat_<double>& lMat, cv::Mat_<double>& rMat, cv::Mat_<int>& nMat) const
  {
    // If this is a null node, return.
    if(node==nullptr)
      return;
      
    // This is not a null node.
    const auto& stage = node->Stage;
    const auto& level = node->Level;
      
    // Record this bit information if it is not a stage-switching node
    if(level>0)
      {
	if(node->ParentBranch->LeftBranch==node)
	  pixelbits[stage][level-1] = false;
	else
	  pixelbits[stage][level-1] = true;
      }
      
    // Is it a pixel bin?
    if(stage==nStages-1 && level==nStageLevels[nStages-1])
      {
	// Sanity check
	CV_Assert(_leftBtree.Search(pixelbits)==node);
	  
	// Inspect the corresponding leaf in the right tree using the given pixelbits.
	if(node->data!=nullptr)
	  {
	    const auto* rnode = _rightBtree.Search(pixelbits);
	    if(rnode!=nullptr)
	      if(rnode->data!=nullptr)
		{
		  // Compute the average coordinates for the left node
		  cv::Mat_<double> xy(1,2);
		  xy(0,0) = 0.; xy(0,1) = 0.;
		  // Number of pixels averaged during correspondence
		  cv::Mat_<int> np(1,2);
		  np(0,0) = static_cast<int>(node->data->size());
		  for(auto& pixel:*(node->data))
		    {
		      xy(0,1) += static_cast<double>(pixel.first);
		      xy(0,0) += static_cast<double>(pixel.second);
		    }
		  xy(0,0) /= np(0,0);
		  xy(0,1) /= np(0,0);
		  lMat.push_back( xy );
		  
		  // Compute the average coordinates for the right node
		  xy(0,0) = 0.; xy(0,1) = 0.; 
		  np(0,1) = static_cast<int>(rnode->data->size());
		  for(auto& pixel:*(rnode->data))
		    {
		      xy(0,1) += pixel.first;
		      xy(0,0) += pixel.second;
		    }
		  xy(0,0) /= np(0,1);
		  xy(0,1) /= np(0,1);
		  rMat.push_back( xy );

		  // Append pixel count
		  nMat.push_back( np );
		}
	  }
      }
    else
      {
	// Inspect its left and right children.
	PixelateCorrespondingLeafNodes(node->LeftBranch, pixelbits, lMat, rMat, nMat);
	PixelateCorrespondingLeafNodes(node->RightBranch, pixelbits, lMat, rMat, nMat);
      }
    return;
  }
    
}

#endif
