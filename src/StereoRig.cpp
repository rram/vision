// Sriramajayam

#include "StereoRig.h"
#include <opencv2/calib3d.hpp>
#include <iostream>

namespace vc
{
  // Default constructor
  StereoRig::StereoRig()
    :_imgSize(0,0) {}

  // Constructor
  StereoRig::StereoRig(const Camera& lcam,
		       const ChessboardCorners& lcorners,
		       const Camera& rcam,
		       const ChessboardCorners& rcorners)
    :_lcam(lcam), _rcam(rcam)
  {
    // Both cameras should be calibrated
    CV_Assert((_lcam.IsCalibrated() && _rcam.IsCalibrated()) &&
	      "\nStereoRig::StereoRig()- Cams not calibrated.\n");

    // Copy image size
    CV_Assert(_lcam.GetImageSize()==_rcam.GetImageSize() &&
	      "\nStereRig::StereoRig()- Inconsistent image size.\n");
    _imgSize = _lcam.GetImageSize();
    
    // chessboard points imaged.
    // 3D points should be the same in both
    const auto& pts3D = lcorners._cornersIn3D;
    const auto& lpts2D = lcorners._cornersInImages;
    const auto& rpts2D = rcorners._cornersInImages;
    
    // Stereo calibration
    const auto& lK = _lcam.GetCameraMatrix();
    const auto& rK = _rcam.GetCameraMatrix();
    const auto& ldistCoeffs = _lcam.GetDistortionCoeffs();
    const auto& rdistCoeffs = _rcam.GetDistortionCoeffs();
    const auto term = cv::TermCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 100, 1.e-5);

    // Calibrate without altering camera instrinsics
    double rmsErr =
      cv::stereoCalibrate(pts3D, lpts2D, rpts2D,
			  lK, ldistCoeffs, rK, rdistCoeffs,
			  _imgSize, _rotMat, _transMat,
			  _essentialMat, _fundamentalMat,
			  cv::CALIB_FIX_INTRINSIC, term); 
    std::cout<<"\nStereocalibration error at stage 1: "<<rmsErr;
    
    // Recalibrate while improving camera intrinsics
    rmsErr =
      cv::stereoCalibrate(pts3D, lpts2D, rpts2D,
			  lK, ldistCoeffs, rK, rdistCoeffs,
			  _imgSize, _rotMat, _transMat,
			  _essentialMat, _fundamentalMat,
			  cv::CALIB_USE_INTRINSIC_GUESS, term);
    std::cout<<"\nStereocalibration error at stage 2: "<<rmsErr;

    // Compute projection matrices
    _lP = cv::Mat::zeros(3,4,CV_64F);
    _rP = cv::Mat::zeros(3,4,CV_64F);

    // Left projection matrix: lK[I|0]
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	_lP.at<double>(i,j) = lK.at<double>(i,j);

    // Right projection matrix: rK[R|t] = [ [rK x R]_{3x3} | [rK x t]_{3x1} ] <- 4x3 matrix.
    double val;
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	{
	  val = 0.;
	  for(int k=0; k<3; ++k)
	    val += rK.at<double>(i,k)*_rotMat.at<double>(k,j);
	  _rP.at<double>(i,j) = val;
	}

    for(int i=0; i<3; ++i)
      {
	val = 0.;
	for(int j=0; j<3; ++j)
	  val += rK.at<double>(i,j)*_transMat.at<double>(j,0);
	_rP.at<double>(i,3) = val;
      }
  }

  
  // Constructor to read stereorig data from a file
  StereoRig::StereoRig(const cv::String xmlfile)
  { Create(xmlfile); }

  
  // Destructor
  StereoRig::~StereoRig() {}
  
  // Create from a file
  void StereoRig::Create(const cv::String xmlfile)
  {
    std::cout<<"\nCreating stereo rig from file "<<xmlfile; std::fflush( stdout );
    
    // Open file with parameters
    cv::FileStorage fs(xmlfile, cv::FileStorage::READ);
    CV_Assert(fs.isOpened() && "\nStereoRig::Create()- Could not open file with stereo parameters.\n");

    // Read image size
    auto fn = fs["image_width"];
    CV_Assert(!fn.empty() && "\nStereoRig::Create()- Could not read image_width field.\n");
    cv::read(fn, _imgSize.width, 0);
    
    fn = fs["image_height"];
    CV_Assert(!fn.empty() && "\nStereoRig::Create()- Could not read image_height field.\n");
    cv::read(fn, _imgSize.height, 0);
    
    CV_Assert((_imgSize.height>0 && _imgSize.width>0) && "\nStereoRig::Create()- Unexpected image sizes.\n");
    
    // Read data for left camera and create it
    fn = fs["lcamera_mat"];
    CV_Assert(!fn.empty() && "\nStereoRig::Create()- Could not read lcamera_mat field.\n");
    cv::Mat lK, ldistCoeffs;
    cv::read(fn, lK);
    fn = fs["lcamera_distcoeffs"];
    CV_Assert(!fn.empty() && "\nStereoRig::Create()- Could not read lcamera_distcoeffs field.\n");
    cv::read(fn, ldistCoeffs);
    _lcam.SetIntrinsics(lK, ldistCoeffs, _imgSize);
    CV_Assert(_lcam.IsCalibrated() && "\nStereoRig::Create()- left camera not calibrated.\n");
    
    // Read data for right camera and create it
    fn = fs["rcamera_mat"];
    CV_Assert(!fn.empty() && "\nStereoRig::Create()- Could not read rcamera_mat field.\n");
    cv::Mat rK, rdistCoeffs;
    cv::read(fn, rK);
    fn = fs["rcamera_distcoeffs"];
    CV_Assert(!fn.empty() && "\nStereoRig::Create()- Could not read rcamera_distcoeffs field.\n");
    cv::read(fn, rdistCoeffs);
    _rcam.SetIntrinsics(rK, rdistCoeffs, _imgSize);
    CV_Assert(_rcam.IsCalibrated() && "\nStereoRig::Create()- right camera not calibrated.\n");
    
    // Read rotation matrix
    fn = fs["rotation_mat"];
    CV_Assert(!fn.empty() && "\nStereoRig::Create()- Could not read rotation_mat field.\n");
    cv::read(fn, _rotMat);
    
    // Read translation matrix
    fn = fs["translation_mat"];
    CV_Assert(!fn.empty() && "\nStereoRig::Create()- Could not read translation_mat field.\n");
    cv::read(fn, _transMat);
    
    // Read essential matrix
    fn = fs["essential_mat"];
    CV_Assert(!fn.empty() && "\nStereoRig::Create()- Could not read essential_mat field.\n");
    cv::read(fn, _essentialMat);
    
    // Read fundamental matrix
    fn = fs["fundamental_mat"];
    CV_Assert(!fn.empty() && "\nStereoRig::Create()- Could not read fundamental_mat field.\n");
    cv::read(fn, _fundamentalMat);

    // Read left camera projection matrix
    fn = fs["lcamera_proj_mat"];
    CV_Assert(!fn.empty() && "\nStereoRig::Create()- Could not read lcamera_proj_mat.\n");
    cv::read(fn, _lP);

    // Read right camera projection matrix
    fn = fs["rcamera_proj_mat"];
    CV_Assert(!fn.empty() && "\nStereoRig::Create()- Could not read rcamera_proj_mat.\n");
    cv::read(fn, _rP);
    
    // Finished reading
    fs.release();
  }


  // Copy constructor
  StereoRig::StereoRig(const StereoRig& obj)
    :_imgSize(obj._imgSize),
     _lcam(obj._lcam), _rcam(obj._rcam), 
     _rotMat(obj._rotMat),
     _transMat(obj._transMat),
     _essentialMat(obj._essentialMat),
     _fundamentalMat(obj._fundamentalMat),
     _lP(obj._lP), _rP(obj._rP) {}

  // Returns the image size
  const cv::Size& StereoRig::GetImageSize() const
  { return _imgSize; }
  
  // Returns camera matrix
  const Camera& StereoRig::GetLeftCamera() const
  { return _lcam; }

  const Camera& StereoRig::GetRightCamera() const
    { return _rcam; }
    
  // Matrices for the stereo-system
  const cv::Mat& StereoRig::GetFundamentalMatrix() const
  { return _fundamentalMat; }
  
  const cv::Mat& StereoRig::GetEssentialMatrix() const
    { return _essentialMat; }
    
  void StereoRig::GetCoordinateMap(cv::Mat& R,
				cv::Mat& T) const
  { R = _rotMat; T = _transMat; }
  
  void StereoRig::GetStereoMatrices(cv::Mat& R, cv::Mat& T,
				 cv::Mat& E, cv::Mat& F) const
  {
    R = _rotMat;
    T = _transMat;
    E = _essentialMat;
    F = _fundamentalMat;
  }
  
  
  // Saves stereo-rig details to a file
  void StereoRig::Save(const cv::String xmlfile) const
  {
    std::cout<<"\nSaving stereo rig to file "<<xmlfile; std::fflush( stdout );
    
    cv::FileStorage fs(xmlfile, cv::FileStorage::WRITE);
    CV_Assert(fs.isOpened() && "\nStereoRig::Save()- Could not open file to write.\n");

    // Write stereo-matrices to file
    fs << "image_width" << _imgSize.width;
    fs << "image_height" << _imgSize.height;
    fs << "rotation_mat" << _rotMat;
    fs << "translation_mat" << _transMat;
    fs << "essential_mat" << _essentialMat;
    fs << "fundamental_mat" << _fundamentalMat;
    fs << "lcamera_mat" << _lcam.GetCameraMatrix();
    fs << "lcamera_distcoeffs" << _lcam.GetDistortionCoeffs();
    fs << "rcamera_mat" << _rcam.GetCameraMatrix();
    fs << "rcamera_distcoeffs" << _rcam.GetDistortionCoeffs();
    fs << "lcamera_proj_mat" << _lP;
    fs << "rcamera_proj_mat" << _rP;
    
    fs.release();
  }
  

  // Camera projection matrices
  void StereoRig::GetProjectionMatrices(cv::Mat& lP, cv::Mat& rP) const
  { lP = _lP; rP = _rP; return; }

  
  // Perform consistency checks on E, F, R, T
  // Checks det(E) = det(F) = 0, det(R) = 1,
  // relationship between E and F,
  // relationship between E, R and T.
  bool StereoRig::ConsistencyTest(const double EPS) const
  {
    const auto& lK = _lcam.GetCameraMatrix();
    const auto& rK = _rcam.GetCameraMatrix();
    const auto& E = _essentialMat;
    const auto& F = _fundamentalMat;
    const auto& R = _rotMat;
    const auto& T = _transMat;

    // Sizes and types
    assert(F.rows==3 && F.cols==3 && F.type()==CV_64F);
    assert(E.rows==3 && E.cols==3 && E.type()==CV_64F);
    assert(R.rows==3 && R.cols==3 && R.type()==CV_64F);
    assert(T.rows==3 && T.cols==1 && T.type()==CV_64F);
    
    // Determinants of E, F and R
    double det = cv::determinant(F);
    if(std::abs(det)>EPS)
      {
	std::cout<<"\nStereoRig::ConsistencyTest()- "
		 <<"\ndet(F) = "<<det<<" exceeded tolerance "<<EPS;
	return false;
      }
    
    det = cv::determinant(E);
    if(std::abs(det)>EPS)
      {
	std::cout<<"\nStereoRig::ConsistencyTest()- "
		 <<"\ndet(E) = "<<det<<" exceeded tolerance "<<EPS;
	return false;
      }

    det = cv::determinant(R);
    if(std::abs(det-1.)>EPS)
      {
	std::cout<<"\nStereoRig::ConsistencyTest()- "
		 <<"\ndet(R) = "<<det<<" exceeded 1 by tolerance "<<EPS;
	return false;
      }

    // Relationship between E and F: E = rK^T F lK.
    double val1, val2;
    cv::Mat Ep(3,3,CV_64F);
    int irow, icol;
    double maxval = E.at<double>(0,0);
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	{
	  auto& val = Ep.at<double>(i,j);
	  val = 0.;
	  for(int k=0; k<3; ++k)
	    for(int L=0; L<3; ++L)
	      val += rK.at<double>(k,i)*F.at<double>(k,L)*lK.at<double>(L,j);
	  
	  // Record the max. element in E and its location
	  if(std::abs(E.at<double>(i,j))>std::abs(maxval))
	    { irow = i; icol = j; maxval = E.at<double>(i,j); }
	}
    
    // E and Ep can differ by a scale factor.
    // They have to be normalized by corresponding matrix elements.
    double maxvalp = Ep.at<double>(irow, icol);
    CV_Assert( (std::abs(maxval)>EPS && std::abs(maxvalp)>EPS) &&
	       "\nStereoRig::ConsistencyTest()- E was close to zero.\n");
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	{
	  val1 = E.at<double>(i,j)/maxval;
	  val2 = Ep.at<double>(i,j)/maxvalp;
	  if(std::abs(val1-val2)>EPS)
	    {
	      std::cout<<"\nStereoRig::ConsistencyTest()- "
		       <<"Check for relation between essential and fundamental matrices failed: "
		       <<"\n"<<val1<<" should be close to "<<val2
		       <<" with tolerance "<<EPS; std::fflush( stdout );
	      return false;
	    }
	}
    
    // E should equal \hat{T} R
    double That[3][3] =
      {{0.,                -T.at<double>(2,0), T.at<double>(1,0)},
       {T.at<double>(2,0),  0.,                -T.at<double>(0,0)},
       {-T.at<double>(1,0), T.at<double>(0,0), 0.}};
    
    for(int i=0; i<3; ++i)
      for(int j=0; j<3; ++j)
	{
	  val1 = E.at<double>(i,j);
	  val2 = 0.;
	  for(int k=0; k<3; ++k)
	    val2 += That[i][k]*R.at<double>(k,j);
	  
	  if(std::abs(val1-val2)>EPS)
	    {
	      std::cout<<"\nStereoRig::ConsistencyTest()- "
		       <<"Check for relation between essential and rotation/translation  matrices failed: "
		       <<"\n"<<val1<<" should be close to "<<val2
		       <<" with tolerance "<<EPS; std::fflush( stdout );
	      return false;
	    }
	}

    // Left projection matrix = lK[I|0]
    for(int i=0; i<3; ++i)
      {
	for(int j=0; j<3; ++j)
	  if(std::abs(lK.at<double>(i,j)-_lP.at<double>(i,j))>EPS)
	    {
	      std::cout<<"\nStereoRig::ConsistencyTest()- "
		       <<"Check for left projection matrix failed: "
		       <<"\n"<<_lP.at<double>(i,j)<<" should be close to "<<lK.at<double>(i,j)
		       <<" with tolerance "<<EPS; std::fflush( stdout );
	      return false;
	    }
	
	// Last column
	if(std::abs(_lP.at<double>(i,3))>EPS)
	  {
	    std::cout<<"\nStereoRig::ConsistencyTest()- "
		     <<"Check for left projection matrix failed: "
		     <<"\n"<<_lP.at<double>(i,3)<<" should be close to 0."
		     <<" with tolerance "<<EPS; std::fflush( stdout );
	    return false;
	  }
      }
    
    
    
    // Right projection matrix:
    double P[3][4];
    for(int i=0; i<3; ++i)
      {
	P[i][3] = 0.;
	for(int j=0; j<3; ++j)
	  {
	    P[i][3] += rK.at<double>(i,j)*T.at<double>(j,0);
	    P[i][j] = 0.;
	    for(int k=0; k<3; ++k)
	      P[i][j] += rK.at<double>(i,k)*R.at<double>(k,j);
	  }
      }
    for(int i=0; i<3; ++i)
      for(int j=0; j<4; ++j)
	if(std::abs(_rP.at<double>(i,j)-P[i][j])>EPS)
	  {
	    std::cout<<"\nStereoRig::ConsistencyTest()- "
		     <<"Check for right projection matrix failed: "
		     <<"\n"<<_rP.at<double>(i,j)<<" should be close to "<<P[i][j]
		     <<" with tolerance "<<EPS; std::fflush( stdout );
	    return false;
	  }

    return true;
  }

}
