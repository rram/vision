// Sriramajayam

#ifndef VISION_BINARY_TREE
#define VISION_BINARY_TREE

#include <BTreeNode.h>
#include <opencv2/core/utility.hpp>

namespace vc
{
 
  // Class for a multi-stage binary tree
  class BinaryTree
  {
  public:
    // Default constructor
    // Create tree with given number of levels
    BinaryTree(const std::vector<int> nstagelevels);
     
    // Destructor: delete root node, which issues a cascading
    // call to delete all nodes in the tree
    virtual ~BinaryTree();

    // Returns the number of stages
    int GetNumStages() const;

    // Returns the number of levels for a stage
    int GetNumLevels(const int stage) const;

    
    // Main functionality: insert data into the tree
    // pixel (X,Y) indices of pixel
    // pixelbits Binary data encoding x,y data
    template<long unsigned int nstages, long unsigned int nmaxlevels>
      void Insert(const Pixel pixel,
		  const PixelBits<nstages, nmaxlevels>& pixelbits);
    
    // Finds the node with given binary data encoding
    template<long unsigned int nstages, long unsigned int nmaxlevels>
      Node* Search(const PixelBits<nstages, nmaxlevels>& pixelbits) const;

    // Creates a node with the given encoding
    template<long unsigned int nstages, long unsigned int nmaxlevels>
      Node* CreateNode(const PixelBits<nstages, nmaxlevels>& pixelbits);

    // Checks if a node with given encoding exists
    template<long unsigned int nstages, long unsigned int nmaxlevels>
      bool DoesNodeExist(const PixelBits<nstages, nmaxlevels>& pixelbits) const;

    // Returns the root node
    Node* GetRootNode() const;
      
  private:
    // Exhaustively create all children under a node
    //void CreateChildren(Node* node);
    
    
    // Root node
    const int nStages;
    const std::vector<int> nStageLevels;
    const int nMaxLevels;
    Node* RootNode;
  };


  // Implementation of class
      
  // Main functionality: finds the node with the given encoding
  template<long unsigned int nstages, long unsigned int nmaxlevels>
    Node* BinaryTree::
    Search(const PixelBits<nstages, nmaxlevels>& pixelbits) const
  {
    CV_Assert(nstages==nStages);
    CV_Assert(nmaxlevels==nMaxLevels);
    
    // Node to be found
    auto* node = RootNode;
    
    // Start from stage 0
    for(int stage=0; stage<nStages; ++stage)
      {
	const auto& bits = pixelbits[stage];

	// Number of levels in this stage
	const auto& nLevels = nStageLevels[stage];
	  
	// Trickle down the levels in this stage
	for(int level=0; level<nLevels; ++level)
	  {
	    // If this bit is 0, proceed left. Else proceed right.
	    if(false==bits.test(level))
	      {
		if(node->LeftBranch==nullptr)
		  return nullptr;
		else
		  node = node->LeftBranch;
	      }
	    else
	      {
		if(node->RightBranch==nullptr)
		  return nullptr;
		else
		  node = node->RightBranch;
	      }
	  }
	
	// Switch stages
	if(stage!=nStages-1)
	  {
	    if(node->LeftBranch==nullptr)
	      return nullptr;
	    else
	      node = node->LeftBranch;
	  }
      }
    
    // Sanity checks.
    CV_Assert(node->LeftBranch==nullptr && node->RightBranch==nullptr);
    CV_Assert(node->Stage==nStages-1);
    CV_Assert(node->Level==nStageLevels[nStages-1]);
    
    // We have found the required node.
    return node;
  }
  
  
  // Main functionality: insert data into the tree
  // pixel (X,Y) indices of pixel
  // pixelbits Binary data encoding x,y data
  template<long unsigned int nstages, long unsigned int nmaxlevels>
    void BinaryTree::Insert(const Pixel pixel,
			    const PixelBits<nstages, nmaxlevels>& pixelbits)
  {
    CV_Assert(nstages==nStages);
    CV_Assert(nmaxlevels==nMaxLevels);
      
    // Locate the node with the given encoding
    auto* node = CreateNode(pixelbits);
    //auto* node = Search(pixelbits);
    CV_Assert(node!=nullptr && "\nvc::BinaryTree::Insert()- Could not locate node.\n");
    CV_Assert((node->Stage==nStages-1 && node->Level==nStageLevels[nStages-1]) &&
	      "\nvc::BinaryTree::Insert()- Unexpected level in leaf node.\n");
    
    // Insert this pixel into the leaf node
    if(node->data==nullptr)
      node->data = new std::vector<Pixel>;
    node->data->push_back( pixel );
    
    return;
  }


  // Creates a node with the given encoding
  template<long unsigned int nstages, long unsigned int nmaxlevels>
    Node* BinaryTree::CreateNode(const PixelBits<nstages, nmaxlevels>& pixelbits)
  {
    CV_Assert(nstages==nStages);
    CV_Assert(nmaxlevels==nMaxLevels);
    
    // Start from the root node
    Node* node = RootNode;
    
    // Trickle down the stages and levels, creating nodes as needed.
    for(int stage=0; stage<nStages; ++stage)
      {
	// Number of levels for this stage
	const auto& nLevels = nStageLevels[stage];

	// Read the bit for this stage
	const auto& bits = pixelbits[stage];
	for(int level=0; level<nLevels; ++level)
	  {
	    const bool val = bits.test(level);
	    if(val==false) // Proceed left
	      {
		// If this node does not exist, create one.
		if(node->LeftBranch==nullptr)
		  {
		    node->LeftBranch = new Node;
		    node->LeftBranch->ParentBranch = node;
		    node->LeftBranch->Stage = node->Stage;
		    node->LeftBranch->Level = node->Level+1;
		  }
		node = node->LeftBranch; // Descend
	      }
	    else // Proceed right
	      {
		if(node->RightBranch==nullptr)
		  {
		    node->RightBranch = new Node;
		    node->RightBranch->ParentBranch = node;
		    node->RightBranch->Stage = node->Stage;
		    node->RightBranch->Level = node->Level+1;
		  }
		node = node->RightBranch; // Descend
	      }
	  }

	// Finished descending this stage
	// We will have to proceed into the next stage
	if(stage<nStages-1)
	  {
	    // Create the next stage along the left branch if necessary
	    if(node->LeftBranch==nullptr)
	      {
		node->LeftBranch = new Node;
		node->LeftBranch->ParentBranch = node;
		node->LeftBranch->Stage = node->Stage+1;
		node->LeftBranch->Level = 0;
	      }
	    node = node->LeftBranch;
	  }
      }

    return node;
  }


  // Checks if a node with given encoding exists
  template<long unsigned int nstages, long unsigned int nmaxlevels>
    bool BinaryTree::DoesNodeExist(const PixelBits<nstages, nmaxlevels>& pixelbits) const
  {
    CV_Assert(nstages==nStages);
    CV_Assert(nmaxlevels==nMaxLevels);
    
    // Node to be found
    auto* node = RootNode;
    if(node==nullptr) return false;
    
    // Start from stage 0
    for(int stage=0; stage<nStages; ++stage)
      {
	const auto& bits = pixelbits[stage];

	// Number of levels in this stage
	const auto& nLevels = nStageLevels[stage];
	  
	// Trickle down the levels in this stage
	for(int level=0; level<nLevels; ++level)
	  {
	    // If this bit is 0, proceed left. Else proceed right.
	    if(false==bits.test(level))
	      {
		if(node->LeftBranch==nullptr) return false;
		else node = node->LeftBranch;
	      }
	    else
	      {
		if(node->RightBranch==nullptr) return false;
		else node = node->RightBranch;
	      }
	  }
	
	// Switch stages
	if(stage!=nStages-1)
	  {
	    if(node->LeftBranch==nullptr)
	      return false;
	    else
	      node = node->LeftBranch;
	  }
      }
    return true;
  }
  
}

#endif
