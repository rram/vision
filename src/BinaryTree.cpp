// Sriramajayam

#include "BinaryTree.h"
#include <algorithm>

namespace vc
{

  // Constructor
  BinaryTree::BinaryTree(const std::vector<int> nstagelevels)
    :nStages(nstagelevels.size()), nStageLevels(nstagelevels),
     nMaxLevels(*std::max_element(nStageLevels.begin(),
				  nStageLevels.end()))
  {
    CV_Assert(nStages==static_cast<int>(nStageLevels.size()));
    for(auto& it:nStageLevels)
      CV_Assert(it>0);
    
    // Create the root node only. Remaining nodes will be added as needed.
    RootNode = new Node;
    RootNode->Level = 0;
    RootNode->Stage = 0;
    //CreateChildren(RootNode);
  }
  
  // Destructor
  BinaryTree::~BinaryTree()
  { if(RootNode!=nullptr) delete RootNode; }

  // Returns the number of stages
  int BinaryTree::GetNumStages() const
  { return nStages; }

  // Returns the number of levels for a stage
  int BinaryTree::GetNumLevels(const int stage) const
  { return nStageLevels[stage]; }
  

  // Returns the root node
  Node* BinaryTree::GetRootNode() const
  { return RootNode; }


  
  // Create children in the tree 
  /*void BinaryTree::CreateChildren(Node* node)
  {
    // Stage and level of this node.
    const auto& stage = node->Stage;
    const auto& level = node->Level;

    // Number of levels for this stage to construct
    const auto& nLevels = nStageLevels[stage];

    // If this is the last level of the current stage
    if(level==nLevels)
      {
	// If this is the last stage, we are done.
	if(stage==nStages-1)
	  return;

	// Otherwise, progress to the next stage.

	// Create left branch
	node->LeftBranch = new Node;
	node->LeftBranch->Stage = stage+1;
	node->LeftBranch->Level = 0;
	node->LeftBranch->ParentBranch = node;
	//std::cout<<"\nCreated root node at stage: "<<node->LeftBranch->Stage
	//	   <<" and level: "<<node->LeftBranch->Level; std::fflush( stdout );
	  
	// Propagate the left branch of the tree
	CreateChildren(node->LeftBranch);

	// Nip the right branch. This is by convention.
	node->RightBranch = nullptr;
      }
    // Otherwise, this is not a leaf node.
    // Create children
    else
      {
	// Create left branch
	node->LeftBranch = new Node;
	node->LeftBranch->Level = level+1;
	node->LeftBranch->Stage = stage;
	node->LeftBranch->ParentBranch = node;
	//std::cout<<"\nCreated left node at stage: "<<node->LeftBranch->Stage
	//	   <<" and level: "<<node->LeftBranch->Level; std::fflush( stdout );
	  

	// Propagate the left branch
	CreateChildren(node->LeftBranch);

	// Create right branch
	node->RightBranch = new Node;
	node->RightBranch->Level = level+1;
	node->RightBranch->Stage = stage;
	node->RightBranch->ParentBranch = node;
	//std::cout<<"\nCreated right node at stage: "<<node->RightBranch->Stage
	//	   <<" and level: "<<node->RightBranch->Level; std::fflush( stdout );
	  
	// Propagate the right branch
	CreateChildren(node->RightBranch);
      }
      
    return;
    }*/

  

}
