// Sriramajayam

#ifndef VC_UTILS_H
#define VC_UTILS_H

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

namespace vc
{
  // Function that takes the image with a projected pattern
  // and the image with the shifted pattern.
  // A new image with a refined pattern is computed
  void ConvertShiftedImage(const std::string imfile,
			   const std::string rev_imfile,
			   const std::string simfile,
			   const std::string rev_simfile,
			   const std::string newfile,
			   const std::string rev_newfile);

  // Function that prints a point set from an xml file to a .ply
  void ConvertPointSetXml2Ply(const cv::String field,
			      const cv::String xmlfile,
			      const cv::String plyfile);

  // Append multiple point set fields from xml file to a .ply
  void ConvertPointSetXmls2Ply(const std::vector<cv::String> fields, 
			       const std::vector<cv::String> xmlfiles,
			       const cv::String plyfile);

  // Identify duplicate images in a folder
  std::vector<std::pair<cv::String, cv::String>>
    IdentifyDuplicateImages(const cv::String folder,
			    const cv::String stem, const cv::String extension,
			    const double TOL);

  // Transform points in a given file with the given field
  void ApplyRigidBodyTransformation(const cv::String rtfile, const cv::String inxmlfile, 
				    cv::String outxmlfile, const cv::String field);
    

}
#endif
