var annotated_dup =
[
    [ "vc", null, [
      [ "BinaryTree", "classvc_1_1_binary_tree.html", "classvc_1_1_binary_tree" ],
      [ "Camera", "classvc_1_1_camera.html", "classvc_1_1_camera" ],
      [ "Charucoboard", "classvc_1_1_charucoboard.html", "classvc_1_1_charucoboard" ],
      [ "CharucoBoardImage", "structvc_1_1_charuco_board_image.html", "structvc_1_1_charuco_board_image" ],
      [ "Chessboard", "classvc_1_1_chessboard.html", "classvc_1_1_chessboard" ],
      [ "ChessboardCorners", "classvc_1_1_chessboard_corners.html", "classvc_1_1_chessboard_corners" ],
      [ "ClickCorresponder", "classvc_1_1_click_corresponder.html", "classvc_1_1_click_corresponder" ],
      [ "Corresponder", "classvc_1_1_corresponder.html", "classvc_1_1_corresponder" ],
      [ "ImageSet", "structvc_1_1_image_set.html", "structvc_1_1_image_set" ],
      [ "Node", "structvc_1_1_node.html", "structvc_1_1_node" ],
      [ "StereoRig", "classvc_1_1_stereo_rig.html", "classvc_1_1_stereo_rig" ],
      [ "Triangulator", "classvc_1_1_triangulator.html", "classvc_1_1_triangulator" ]
    ] ]
];