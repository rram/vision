var structvc_1_1_node =
[
    [ "Node", "structvc_1_1_node.html#a9f25302787cd85ca2b263ffa39a65362", null ],
    [ "~Node", "structvc_1_1_node.html#a6b01599d8776b6cf68f8ba1b653adcc6", null ],
    [ "data", "structvc_1_1_node.html#a97640bd7304540b0af9e4cb144baf3a5", null ],
    [ "LeftBranch", "structvc_1_1_node.html#a7ad9ea62c1ed5ccd391d640e7ee8baec", null ],
    [ "Level", "structvc_1_1_node.html#a8d8de88a8f48df13eb2c81c1a36d0ae2", null ],
    [ "ParentBranch", "structvc_1_1_node.html#ae855aa379d58c24a24d08b494d14ef71", null ],
    [ "RightBranch", "structvc_1_1_node.html#ac1fc292e6482f86241bea6f3d02c9593", null ],
    [ "Stage", "structvc_1_1_node.html#ab6213f18c5612bfc4bf627f427e20ab1", null ]
];